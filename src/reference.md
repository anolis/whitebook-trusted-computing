# 参考资料
[1]冯登国,刘敬彬,秦宇等,创新发展中的可信计算理论与技术U].中国科学:信息科学,2020,50(08):1127-1147.<br>
[2]沈昌祥,公备.基于国产密码体系的可信计算体系框架盯.密码学报,2015,2(05):381-389.DO1:10.13868/j.<br>
[3]石文昌编著.信息系统安全概论,电子工业出版社，2014.68-71.<br>
[4]张焕国、赵波等著.可信计算.武汉大学出版社.<br>
[5]威尔·亚瑟(WillArthur)/大卫·查林纳(David Challener)等《TPM 2.0原理及应用指南》.机械工业出版社.2017-10-1.<br>
[6]王勇,张雨菡,洪智等.基于TPM 2.0 的内核完整性度量框架川.计算机工程,2018,44(03):166-170+177.<br>
[7]https://www.intel.com/content/www/us/en/developer/articles/codesample/protecting-secret-data-and-keys-using-intel-platform-trust-technology.html.<br>
[8]沈昌祥院士《可信计算筑牢网络强国底座》演讲稿.<br>
[9]浪潮信息张东于 2022年Keyarch0s发布会.<br>