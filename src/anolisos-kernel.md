# 3.2 内核可信特性
## 3.2.1 概述
龙蜥操作系统内核提供了成熟的可信计算特性支持，满足内核安全可信需求、为应用层提供安全可信服务。
## 3.2.2 可信驱动
### TPM Eventlog
本节简要描述了什么是TPM Eventlog,以及如何将TPM Eventlog从预引导固件传递到操作系统，事件中一般也将TPM Eventlog称为度量日志。
1. **TPM Eventlog概述**<br>
预引导固件维护一个TPM Eventlog，每当固件度量新的组件并将度量结果扩展到任何PCR寄存器时，该日志都会新增一条记录。按事件类型分隔，并包含了扩展后的新PCR值。TPM Eventlog的主要用于远程证明，用于向挑战者证明平台的可信状态。度量日志（即TPM Eventlog)的主要作用是向挑战提供详细的度量日志、来证明详细的度量对象和度量结果，远程证明过程中通过包含PCR内容的可信报告验证度量日志的可信性。
2. **UEFI TPM Eventlog**<br>
UEFI提供的 TPM Eventlog查询的protocol服务。在调用ExitBootServices()之前，Linux EFI存根将事件日志复制到由内核自定义的配置表中。<br>
需要说明的是，ExitBootServices()生成的度量结果不会出现在最终的度量日志表中。固件提供了最终事件配置表来解决这个问题。在第一次调用EFI_TCG2_PROTOCOL.GetEventLog()之后，事件被镜像到这个表中。
###  vTPM Proxy Driver for linux Container 
1） **面向容器的虚拟TPM驱动概述**<br>
该特性是为每个Linux容器提供TPM功能，该特性允许程序以与物理系统上TPM交互相同的方式与容器中的TPM交互。每个容器都有自己独特的、模拟的软件TPM。<br>
2） **面向容器的虚拟TPM驱动的设计**<br>
为了使模拟swTPM可用于每个容器，容器管理堆栈需要创建一个设备对，该设备对由一个客户端TPM字符设备/dev/tpmX (X=0,1,2…)和一个“服务器端”文件描述符组成。在将文件描述符传递给TPM仿真器的同时，通过创建具有适当主编号和副编号的字符设备将前者移动到容器中。然后，容器内的软件可以使用字符设备发送TPM命令，仿真器将通过文件描述符接收命令，并使用它发送回响应。<br>
虚拟TPM代理驱动程序提供了一个设备/dev/vtpmx，用于使用ioctl创建设备对。ioctl将其作为配置设备的输入标志。例如，这些标志指示TPM模拟器是否支持TPM 1.2或TPM 2功能。ioctl的结果是“服务器端”的文件描述符以及所创建的字符设备的主次编号。此外，还会返回TPM字符设备的编号。例如，如果创建了/dev/tpm10，则返回数字(dev_num) 10。 <br>
一旦设备被创建，驱动程序将立即尝试与TPM通信。驱动程序的所有命令都可以从ioctl返回的文件描述符中读取。这些命令应该立即得到响应。<br>
3） **UAPI**<br>
-  flags for the proxy TPM
```C
enum vtpm_proxy_flags
/**
*  常量
*  VTPM_PROXY_FLAG_TPM2
*       the proxy TPM uses TPM 2.0 protocol
**/
```
- parameter structure for the VTPM_PROXY_IOC_NEW_DEV ioctl
```C
struct vtpm_proxy_new_dev

//Definition:
struct vtpm_proxy_new_dev {
    __u32 flags;
    __u32 tpm_num;
    __u32 fd;
    __u32 major;
    __u32 minor;
};
/**结构体成员说明
*  flags: flags for the proxy TPM
*  tpm_num: index of the TPM device
*  fd: the file descriptor used by the proxy TPM
*  major: the major number of the TPM device
*  minor: the minor number of the TPM device
**/
```
- handler for the VTPM_PROXY_IOC_NEW_DEV ioctl
```C
long vtpmx_ioc_new_dev(struct file *file, unsigned int ioctl, unsigned long arg)

/**函数参数说明
*  struct file *file     /dev/vtpmx
*  unsigned int ioctl    the ioctl number
*  unsigned long arg     pointer to the struct vtpmx_proxy_new_dev
**/

/*函数功能描述
* 创建一个匿名文件，供进程作为TPM与客户端进程通信。
* 该函数还将添加一个新的TPM设备，通过该设备将数据代理到该TPM代理进程。
* 将为调用者提供一个文件描述符，用于与客户端通信，以及TPM设备的主要和次要编号。
*/
```
### vTPM interface for Xen
本文档描述Xen的虚拟可信平台模块(vTPM)子系统。假定读者熟悉构建和安装Xen、Linux，并对TPM和vTPM概念有基本的了解。
**介绍** <br>
这项工作的目标是为虚拟客户机操作系统(用Xen术语来说就是DomU)提供TPM功能。这允许程序以与物理系统上的TPM交互相同的方式与虚拟系统中的TPM交互。每个客户机都有自己独特的、仿真的软件TPM。然而，每个vTPM的秘密(密钥，NVRAM等)都由vTPM Manager域管理，该域将秘密密封到物理TPM。如果创建这些域(管理器、vTPM和客户机)的过程是可信的，那么vTPM子系统将根植于硬件TPM的信任链扩展到Xen中的虚拟机。vTPM的每个主要组件都作为一个单独的域实现，提供由hypervisor保证的安全分离。vTPM域在mini-o中实现，以减少内存和处理器开销。
这个mini-os vTPM子系统是建立在IBM和Intel公司以前的vTPM工作之上的。
**设计概述** <br>
vTPM的架构描述如下:
```
+------------------+
|    Linux DomU    | ...
|       |  ^       |
|       v  |       |
|   xen-tpmfront   |
+------------------+
        |  ^
        v  |
+------------------+
| mini-os/tpmback  |
|       |  ^       |
|       v  |       |
|  vtpm-stubdom    | ...
|       |  ^       |
|       v  |       |
| mini-os/tpmfront |
+------------------+
        |  ^
        v  |
+------------------+
| mini-os/tpmback  |
|       |  ^       |
|       v  |       |
| vtpmmgr-stubdom  |
|       |  ^       |
|       v  |       |
| mini-os/tpm_tis  |
+------------------+
        |  ^
        v  |
+------------------+
|   Hardware TPM   |
+------------------+
```
- Linux DomU:想要使用vTPM的基于Linux的客户机。可能不止一个。
- xen-tpmfront.ko:Linux内核虚拟TPM前端驱动。该驱动程序提供对基于linux的DomU的vTPM访问。
- mini-os / tpmback:Mini-os TPM后端驱动程序。Linux前端驱动与后端驱动对接，实现Linux DomU与vTPM之间的通信。这个驱动程序也被vtpmmgr-stubdom用来与vtpm-stubdom通信。
- vtpm-stubdom:一个实现vTPM的mini-os存根域。在系统上运行的vtpm-stubdom实例和逻辑vtpms之间存在一对一的映射关系。vTPM平台配置寄存器(pcr)通常都初始化为零。
- mini-os / tpmfront:Mini-os TPM前端驱动程序。vTPM mini-os域vTPM -stubdom使用该驱动程序与vtpmmgr-stubdom通信。该驱动程序也用于与vTPM域通信的mini-os域，例如pv-grub。
- vtpmmgr-stubdom:实现vTPM管理器的mini-os域。只有一个vTPM管理器，它应该在机器的整个生命周期中运行。该域规范对系统物理TPM的访问，并确保每个vTPM的持久状态。
- mini-os / tpm_tis: Mini-os TPM version 1.2 TPM Interface Specification (TIS)驱动程序。vtpmmgr-stubdom使用这个驱动程序直接与硬件TPM对话。通过将硬件内存页映射到vtpmmgr-stubdom，方便了通信。
- 硬件TPM:物理TPM模块，焊接到主板上。
**与Xen集成**
Xen在4.3中使用libxl工具栈添加了对vTPM驱动程序的支持。有关设置vTPM和vTPM Manager存根域的详细信息，请参阅Xen文档(docs/misc/ vTPM .txt)。一旦存根域开始运行，就会按照与域配置文件中的磁盘或网络设备相同的方式设置vTPM设备。
为了使用IMA等需要在initrd之前加载TPM的特性，必须将xen-tpmfront驱动程序编译到内核中。如果不使用这些特性，驱动程序可以被编译为一个模块，并将像往常一样加载。
 
### Firmware TPM Driver
本文档介绍了固件的fTPM (Trusted Platform Module)设备驱动程序。
**介绍**
这个驱动程序是在ARM的TrustZone环境中实现的固件的垫片。驱动程序允许程序以与硬件TPM交互相同的方式与TPM交互。
**设计**
驱动程序充当一个薄层，向固件中实现的TPM传递命令。驱动程序本身不包含太多逻辑，更像是固件和内核/用户空间之间的哑管道。
固件本身基于以下文件:https://www.microsoft.com/en-us/research/wp-content/uploads/2017/06/ftpm1.pdf 
当驱动程序加载时，它将向用户空间公开/dev/tpmX字符设备，这将使用户空间能够-通过该设备与固件TPM通信。

## 3.2.3 IMA
### 介绍
原始的“ima”模板是固定长度的，包含了文件数据的哈希值和路径名。filedata哈希值限制为20字节(md5/sha1)。路径名是一个以空结尾的字符串，限制为255个字符。为了克服这些限制并添加额外的文件元数据，它是扩展当前版本的IMA是必要的模板。例如，可能报告的信息是索引节点UID/GID或索引节点和进程的LSM标签这就是访问它。然而，引入这个特性的主要问题是，每次定义一个新的模板，生成和显示函数度量列表将包括处理新格式的代码因此，随着时间的推移，它会显著增长。<br>
提出的解决方案通过分离模板解决了这个问题管理从剩余的IMA代码。这个解决方案的核心是定义两个新的数据结构:一个模板描述符，确定测量表中应包括哪些信息;一个模板字段，以生成和显示给定类型的数据。<br>
使用这些结构管理模板非常简单。支持一种新的数据类型，开发人员定义字段标识符并实现两个函数init()和show()分别用于生成和显示测量条目。定义一个新的模板描述符需要指定模板格式(由字段标识符分隔的字符串)通过' ' ima_template_fmt ' '内核命令行输入' ' | ' '字符)参数。在引导时，IMA初始化所选的模板描述符通过将格式转换为模板字段结构的数组从支撑点的集合中。<br>
在初始化步骤之后，IMA将调用ima_alloc_init_template()(在补丁中为新模板管理定义了新功能机制)，通过使用模板生成新的度量条目描述符通过内核配置或通过new引入' ' ima_template ' '和' ' ima_template_fmt ' '内核命令行参数。<br>
正是在这个阶段，新体系结构的优势体现出来了清楚地显示:后一个函数将不包含要处理的特定代码一个给定的模板，但它只是调用模板的' ' init() ' '方法与所选模板描述符关联的字段，并存储结果(指向已分配数据和数据长度的指针)。<br>
使用相同的机制来显示度量条目。<br>
函数``ima[_ascii]_measurements_show()``检索每个条目，用于生成该条目并调用show()的模板描述符方法，用于模板字段结构数组的每个项。

### 支持的模板字段和描述符
下面是支持的模板字段列表``('&lt;identifier&gt;': description)``，可用于定义新的模板将描述符的标识符添加到格式字符串中(稍后将添加对更多数据类型的支持):
- 'd':事件的摘要(即测量文件的摘要);用SHA1或MD5哈希算法计算;
- 'n':事件的名称(即文件名)，大小为255字节;
- 'd-ng':事件的摘要，用任意散列计算算法(字段格式:&lt;hash算法&gt;:digest);
- 'd-ngv2':与d-ng相同，但前缀为"ima"或"verity"摘要类型(字段格式:&lt;摘要类型&gt;:&lt;散列算法&gt;:摘要);
- 'd-modsig':事件摘要，不包含附加的modsig;
- 'n-ng':事件的名称，没有大小限制;
- 'sig':文件签名，基于文件的/ fsversity的摘要[1]，或EVM便携式签名，如果是安全的。Ima '包含一个文件散列。
- 'modsig'附加文件签名;
- 'buf':用于生成哈希的缓冲区数据，没有大小限制;
- 'evmsig': EVM便携签名;
- 'iuid':索引节点UID;
- 'igid':索引节点的GID;
- 'imode':索引节点模式;
- 'xattrnames': xattr名称列表(以' ' | ' '分隔)，仅当xattr为现在;
- 'xattrlength ': xattr长度列表(u32)，仅当xattr存在时;
- 'xattrvalues': xattr值的列表;

**下面是定义的模板描述符列表:**
- “ima”:格式为“d|n”;
- “ima-ng”(默认):它的格式是“d-ng | n-ng ';
- “ima-ngv2”:格式为“d-ngv2|n-ng”;
- “image -sig”:格式为“d-ng|n-ng|sig”;
- “ima-sigv2”:格式为“d-ngv2|n-ng|sig”;
- “ima-buf”:格式为“d-ng|n-ng|buf”;
- “ima-modsig”:格式为“d-ng|n-ng|sig|d-modsig|modsig”;
- "evm-sig":格式为' ' d-ng|n-ng|evmsig|xattrnames|xattrlength |xattrvalues|iuid|igid|imode ' ';
### IMA使用
要指定用于生成度量条目的模板描述符，请目前支持以下方法:
- 从内核支持的模板描述符中选择一个配置(``ima-ng``是默认选择);
- 指定模板描述符名称从内核命令行通过``ima_template=``参数;
- 注册一个新的模板描述符与自定义格式通过内核命令行参数``ima_template_fmt=``。

## 3.2.4 内核可信密钥与加密密钥支持
可信密钥和加密密钥是添加到现有内核密钥环服务中的两种新密钥类型。这两种新类型都是可变长度对称密钥，在这两种情况下，所有密钥都是在内核中创建的，用户空间只能看到、存储和加载加密的blob。可信密钥需要可信源的可用性以获得更高的安全性，而加密密钥可以在任何系统上使用。为了方便起见，所有用户级blob都以十六进制ASCII格式显示和加载，并经过完整性验证。
### 信任源
可信源为可信密钥提供安全来源。信任源是否足够安全取决于其实现的强度和正确性，以及特定用例的威胁环境。由于内核不知道环境是什么，也没有信任度量，因此它依赖于可信密钥的使用者来确定信任源是否足够安全。
内核支持硬件可信根（如TPM/TCM）作为内核可信密钥的可信源。由于TPM/TCM提供的SRK机制确保了根密钥不会离开可信根、且提供芯片级安全保障机制和高安全等级的密钥熵源。
### 将密钥使用与平台完整性状态绑定
基于TPM/TCM提供的密钥服务，密钥可以选择性地密封到指定的PCR值，并且只有在PCR和blob可信验证通过的情况下，TPM才会对密钥进行解封。加载的可信密钥可以使用新的(未来的)PCR值更新，因此密钥很容易迁移到新的PCR值，例如当内核和initramfs更新时。同一个密钥在不同的PCR值下可以保存多个blob，因此很容易支持多个boot。
### 接口和API
内核支持TPM/TCM访问接口和API

## 3.2.5 内核可信配置参数(KConfig)
内核中有需要可信计算相关的配置参数，下表中列举出部分常用的参数以及解释其含义
| 内核可信配置名称 | 用途 | 依赖的Config(以下省略`CONFIG_`开头) |
| :-: | :-: | :-: |
| CONFIG_SYSTEM_TRUSTED_KEYS | 系统可信keys, 可以级联 | CRYPTO [=y] && SYSTEM_TRUSTED_KEYRING [=y] |
| CONFIG_SECONDARY_TRUSTED_KEYRING | 允许用户从用户空间加入system trusted key，要求key必须是从用户空间加载，且必须由其中一把key签过 | CRYPTO [=y] && SYSTEM_TRUSTED_KEYRING [=y] |
| CONFIG_SYSTEM_EXTRA_CERTIFICATE | 设置预留区域允许用户手动插入证书而不需要重编内核 | CRYPTO [=y] && SYSTEM_TRUSTED_KEYRING [=y] |
| CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE | 证书预留区域的大小 | CRYPTO [=y] && SYSTEM_EXTRA_CERTIFICATE [=y] |
| CONFIG_MODULE_SIG | 开启后对内核模块的签名进行检查 | MODULES [=y] |
| CONFIG_MODULE_SIG_FORCE | 开启后内核会直接拒绝加载签名有问题的内核模块 | MODULES [=y] && MODULE_SIG [=y] |
| CONFIG_INTEGRITY_AUDIT | 使能完整性审计支持 | INTEGRITY [=y] && AUDIT [=y] |
| CONFIG_IMA | 使能IMA  | INTEGRITY [=y] |
| CONFIG_IMA_WRITE_POLICY | 允许对 IMA 策略的多次写入 | INTEGRITY [=y] && IMA [=y] |
| CONFIG_IMA_READ_POLICY | 允许读当前 IMA 策略  | INTEGRITY [=y] && IMA [=y] |
| CONFIG_IMA_X509_PATH | IMA x509证书路径 | INTEGRITY [=y] && IMA_LOAD_X509 [=y] |
| CONFIG_IMA_DEFAULT_HASH | IMA缺省的HASH算法 | INTEGRITY [=y] && IMA [=y] |
| CONFIG_IMA_DEFAULT_TEMPLATE | IMA缺省的模板 |  INTEGRITY [=y] && IMA [=y] |
| CONFIG_IMA_APPRAISE | 使能本地测量完整性评估 | INTEGRITY [=y] && IMA [=y] |
