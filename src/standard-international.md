# 2.2 国际可信计算标准
## 2.2.1 ISO发布 
### ISO简介 
&emsp;&emsp;ISO(国际标准化组织)和IEC(国际电工委员会)组成了专门的全球标准化体系。作为ISO或IEC成员的国家机构通过各自组织建立的技术委员会参与国际标准的制定，以处理特定领域的技术活动。ISO和IEC技术委员会在共同感兴趣的领域进行合作。与ISO和IEC保持联系的其他政府和非政府国际组织也参加了这项工作。在信息技术领域，ISO和IEC成立了一个联合技术委员会ISO/IEC JTC 1。国际标准是根据ISO/IEC指令第2部分中给出的规则起草的。联合技术委员会的主要任务是制定国际标准。联合技术委员会通过的国际标准草案分发给国家机构表决。作为国际标准出版需要至少75%的国家机构投票通过。  
### ISO/IEC 11889系列标准概述
&emsp;&emsp;TPM2.0库规范系列标准以ISO/IEC 11889:2015发布。该规范由可信计算组(TCG)提交给ISO/IEC JTC 1*，遵循JTC 1公开可用规范(PAS)转换过程。 
在最终的TPM 2.0标准化投票中，来自发达经济体和新兴经济体的支持，赞成票来自澳大利亚、比利时、加拿大、中国、捷克、丹麦、芬兰、法国、加纳、爱尔兰、意大利、日本、韩国、黎巴嫩、马来西亚、荷兰、尼日利亚、挪威、俄罗斯联邦、南非、阿拉伯联合酋长国、英国和美国。<br>
&emsp;&emsp;TPM 2.0库规范支持现代安全和隐私保护，该规范整合了基于硬件和基于软件的安全技术的优势。它在计算设备中的实现保护了加密密钥;防止私钥被导出;屏蔽用于身份验证的PIN值;并记录和匿名报告在启动过程中加载的软件，以防止恶意软件和攻击。因此，它将成为任何综合安全战略的重要组成部分。<br>
![TPM标准](images/iso_TPM_library_standard.png)  

**ISO/IEC 11889系列标准**

- [ISO/IEC 11889-1:2015 Information technology — TPM Library — Part 1: Architecture](https://www.iso.org/standard/66510.html)
- [ISO/IEC 11889-2:2015 Information technology — TPM Library — Part 2: Structures](https://www.iso.org/standard/66511.html)
- [ISO/IEC 11889-3:2015 Information technology — TPM Library — Part 3: Commands](https://www.iso.org/standard/66512.html)
- [ISO/IEC 11889-4:2015 Information technology — TPM Library — Part 4: Supporting Routines](https://www.iso.org/standard/66513.html) 
## 2.2.2 TCG发布
### 关于TCG
&emsp;&emsp;可信计算组（Trusted Computing Group，TCG）是一个非营利组织，旨在为可互操作的可信计算平台开发、定义和推广基于硬件信任根的供应商无关的全球行业规范和标准。TCG的核心技术包括可信平台模块（Trusted Platform Module，TPM），可信网络通信（Trusted Network Communications，TNC）以及网络安全和自加密驱动器的规范和标准。TCG还设有工作组，将信任的核心概念从企业扩展到物联网的云安全、虚拟化和其他平台以及计算服务。  
![TCG Work Group](images/TCG工作组.png) 

### TPM2.0库标准
&emsp;&emsp;TPM2.0库标准是TPM2.0中基础规范，该规范描述了TPM2.0实现的所有核心功能。库标准由四部分组成：
- 第一部分：架构规范，该部分描述了TPM2.0的操作规范、设计原理及工作细节（例如如何创建用于授权、审计、加密命令的会话）。
- 第二部分：数据结构规范，该部分描述了TPM2.0所使用的数据类型、结构体、联合体，该部分也包含了TPM所使用的命令码、错误码等内容。
- 第三部分：命令规范，该部分描述了TPM2.0中所使用的命令及其功能使用方法，包括了命令的输入输出参数、参考的错误码含义，并提供了命令在TPM中实现的伪代码。
- 第四部分： 支持例程，该部分描述了TPM2.0的实现细节（可以作为tpm模拟器实现的设计参考）。

[TCG TPM库标准下载官网入口](https://trustedcomputinggroup.org/work-groups/trusted-platform-module/)
-   [Part 1: Architecture](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part1_Architecture_pub.pdf)
-   [Part 2: Structures](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part2_Structures_pub.pdf)
-   [Part 3: Commands](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part3_Commands_pub.pdf)
-   [Part 3: Commands – Code](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part3_Commands_code_pub.pdf)
-   [Part 4: Supporting Routines](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part4_SuppRoutines_pub.pdf)
-   [Part 4: Supporting Routines – Code](https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part4_SuppRoutines_code_pub.pdf) 
### TPM Software Stack（TSS）标准
&emsp;&emsp;TSS (TCG Software Stack)是一种软件规范，提供了访问TPM功能的标准API,TSS由多层组成，允许对可扩展的TSS实现进行定制，以适应高端系统和资源受限的低端系统(如下图所示)。TSS也设计了应用程序提供与本地或远程TPM通信的接口方法。  

![TCG Software Stack 2.0](images/tss2.0spec-arch.PNG)  

应用程序开发人员可以参考该规范开发可互操作的客户端应用程序，以实现对可信计算能力的调用。TSS的另一个作用是将程序程序员与与TPM接口的底层细节隔离开来、降低TPM应用开发的学习成本。
- [TSS规范官网入口](https://trustedcomputinggroup.org/resource/tcg-software-stack-tss-specification/)
### PC Client
&emsp;&emsp;PC Client系列规范主要定义了TCG技术在计算机(例如笔记本电脑、台式机或平板电脑)中的上下文中所呈现的功能和行为，包括TPM以及与TPM交互或在其平台中集成TPM的平台OEM和固件供应商所需参考的规范、技术要求和指导。主要分为PC Client TPM Platform标准及配套参考文档和PC Client Firmware标准及配套参考文档。

PC Client TPM Platform标准及配套文档体系:  
![PC Client TPM Platform](images/pcclient_spec&resources.png)  

<font color="gray">__说明：附图源自TCG官网__</font>  

**缩略语**
- PTP – Platform TPM Profile
- CRB – Command Response Buffer interface
- DDWG – Device Driver’s Writers Guide
- Certification PP – Certification Protection Profile
- TIS – TPM Interface Specification

PC Client Firmware标准及配套文档体系:  
![PC Client TPM Firmware](images/pcclient_firmware_spe&resources.png)  

<font color="gray">__说明：附图源自TCG官网__</font>  

**缩略语**
- PFP – Platform Firmware Profile
- PPI – Physical Presence Interface
- FIM – Firmware Integrity Measurement
- MOR – Reset Attack Mitigation – Memory on reset attack mitigation
- RIM – Reference Integrity Manifest
- DRTM – Dynamic Root of Trust for Measurement


| 类型 | 标准名称/案例 | 最新版本及发布时间 | 标准概述 |
|---------|--------|---------|----------------------------|
|PC Client TPM Platform|TCG PC Client Platform TPM Profile (PTP) Specification for TPM2.0|v1.05 Rrvision14 2020.9.4|A TPM claiming adherence to this specification SHALL be compliant with the TPM Library Specification|
|PC Client TPM Platform|TCG TPM I2C Interface Specification for TPM2.0|Revision 1.0 2016.10.7|The Trusted Computing Group TPM I2C Interface Specification is an industry specification that defines an I2C Interface for TPM 2.0.|
|PC Client Firmware|TCG PC Client Specific Platform Firmware Profile Specification|Version 1.05 Revision 23 2021.5.7|The PC Client Platform Specific Profile for TPM 2.0 systems defines the requirements for platform firmware to initialize and interact with a TPM 2.0 device in a PC Client platform|
|PC Client Firmware|TCG EFI Protocol Specification for TPM2.0|Version1.0 Revision 0.13 2016.3.30| It defines data structures and APIs that allow an OS to interact with UEFI firmware to query information important in an early OS boot stage. Such information include: is a TPM present, which PCR banks are active, change active PCR banks, obtain the TCG boot log, extend hashes to PCRs, and append events to the TCG boot log.|
|PC Client Firmware|TCG Platform Reset Attack Mitigation Specification|Version 1.1 Revision 17 2019.2.21|When a platform reboots or shuts down, the contents of volatile memory (RAM) are not immediately lost. Without an electric charge to maintain the data in memory, the data will begin to decay. During this period, there is a short timeframe during which an attacker can turn off or reboot the platform, and quickly turn it back on to boot into a program that dumps the contents of memory. Encryption keys and other secrets can be easily compromised through this method.|
|PC Client Firmware|TCG PC Client Platform Firmware Integrity Measurement|Version 1.0 Revision 43 2021.5.7|This document describes the requirements for a PC Client Endpoint in an enterprise computing environment complying with SP 800-155 BIOS Integrity Measurements|
|PC Client Firmware|TCG PC Client Physical Presence Interface Specification|Version 1.30 Revision 0.52 2015.7.28|This specification defines an interface between an operating system and the firmware to manage the configuration of a TPM and, if required, initiate TPM related operations. The specification gives suggestions on UI wording for interactions with users of a system, if UI interaction is required.|
### Storage
&emsp;&emsp;存储工作组以现有的TCG技术和理念为基础，重点关注专用存储系统上的安全服务标准。其中一个目标是开发标准和实践，用于跨专用存储控制器接口定义相同的安全服务，包括但不限于ATA、串行ATA、SCSI、FibreChannel、USB存储、IEEE 1394、网络附加存储(TCP/IP)、NVM Express和iSCSI。存储系统包括磁盘驱动器、可移动媒体驱动器、闪存和多个存储设备系统。
存储标准体系:  
![Storage Spec Category](images/iso_TPM_library_standard.png)  

### TNC（可信网络通信）
&emsp;&emsp;TCG的可信网络通信(TNC-Trusted Network Communications)工作组定义并发布了一个开放架构和一套不断增长的网络安全标准，在跨各种端点、网络技术和策略的多供应商环境中提供可互操作的端到端信任。TNC支持在不同的网络和安全系统之间进行端点遵从性评估、智能策略决策、动态安全实施和安全自动化。
- 标准编制
TNC工作组已经定义并发布了一个开放架构和一套不断增长的标准，用于端点遵从性评估、网络访问控制和安全自动化。TNC体系结构使网络运营商能够在网络连接和跨不同安全和网络设备的协调信息时或之后执行有关端点完整性的策略。

|一级标准|子标准|
|-------|-----|
|TNC Architecture for Interoperability|N/A|
|IF-IMC – Integrity Measurement Collector Interface|N/A|
|IF-IMV – Integrity Measurement Verifier Interface|N/A|
|IF-TNCCS – Trusted Network Connect Client-Server Interface|IF-TNCCS: TLV Binding|
|IF-TNCCS – Trusted Network Connect Client-Server Interface|IF-TNCCS: Protocol Bindings for SoH|
|IF-M – Vendor-Specific IMC/IMV Messages Interface|IF-M: TLV Binding|
|IF-M – Vendor-Specific IMC/IMV Messages Interface|SWID Message and Attributes for IF-M|
|IF-T – Network Authorization Transport Interface|IF-T: Protocol Bindings for Tunneled EAP Methods|
|IF-T – Network Authorization Transport Interface|IF-T: Binding to TLS|
|IF-PEP – Policy Enforcement Point Interface|IF-PEP: Protocol Bindings for RADIUS|
|IF-MAP – Metadata Access Point Interface|IF-MAP Binding for SOAP|
|IF-MAP – Metadata Access Point Interface|IF-MAP Metadata for Network Security|
|IF-MAP – Metadata Access Point Interface|IF-MAP Metadata for ICS Security|
|IF-MAP – Metadata Access Point Interface|MAP Content Authorization|
|ECP – Endpoint Compliance Profile|N/A|
|CESP – Clientless Endpoint Support Profile|N/A|
|Server Discovery and Validation|N/A|
|Federated TNC|N/A|
|IF-PTS – Platform Trust Services Interface|Attestation PTS Protocol: Binding to IF-M|
|IF-PTS – Platform Trust Services Interface|Simple Object Schema|
|IF-PTS – Platform Trust Services Interface|Core Integrity Schema|
|IF-PTS – Platform Trust Services Interface|Integrity Report Schema|
|IF-PTS – Platform Trust Services Interface|Reference Manifest (RM) Schema|
|IF-PTS – Platform Trust Services Interface|Security Qualities Schema|
|IF-PTS – Platform Trust Services Interface|Verification Result Schema| 

- 应用场景
TNC不同场景下的安全需求提供了可互操作的标准，TNC标准确保跨各种端点、网络技术和策略的多供应商互操作性：  
 
|需求场景|要求|解决的问题|
|-------|--------|----|
|合规遵从|网络和端点可见性|谁和什么都在我的网络上?
|合规遵从|端点遵从性|我网络上的设备安全吗?|
|合规遵从|端点遵从性|用户/设备行为是否合适?|
|访问控制|网络实施|阻止未经授权的用户、设备或行为|
|访问控制|网络实施|对授权用户/设备授予适当的访问权限|
|安全调度|安全系统协调|共享用户、设备、威胁等实时信息。|

- 标准推广：
TNC标准的采用也从供应商和最终用户扩展到其他标准组织。互联网工程任务组(IETF)网络端点评估(NEA)工作组发布了几个基于TNC客户端-服务器协议的rfc:  

|IETF RFC|TNC Specification|
|--------|-----------------|
|PA-TNC: A Posture Attribute (PA) Protocol Compatible with Trusted Network Connect (TNC) - RFC 5792|TNC IF-M: TLV Binding Version 1.0|
|PB-TNC: A Posture Broker (PB) Protocol Compatible with Trusted Network Connect (TNC) - RFC 5793|	TNC IF-TNCCS: TLV Binding Version 2.0|
|A Posture Transport Protocol over TLS (PT-TLS) - RFC 6876|	TNC IF-T Binding to TLS Version 2.0|
|PT-EAP: Posture Transport (PT) Protocol for Extensible Authentication Protocol (EAP) Tunnel Methods - RFC 7171|TNC IF-T: Protocol Bindings for Tunneled EAP Methods, Version 2.0| 

&emsp;&emsp;TNC提供了一个灵活、开放的体系结构，可以适应不断变化的环境，而不依赖于任何一家供应商。跨国公司支持的技术提高了投资回报率，支持使用现有的网络设备和同类最佳产品，并避免了供应商锁定。可见性和协调性有助于有效的网络管理和安全。
TNC开放的网络安全架构和完整的标准得益于安全专家的全面技术审查。为了获得最强的安全性，TNC可以利用TPM进行健壮的身份验证、认证和危害检测。商业供应商、开源社区和IETF对TNC标准提供了广泛的支持。
TNC可以与TPM集成，以实现安全身份验证和认证，解决rootkit和其他受损软件的检测和缓解问题。TNC标准为保护嵌入式系统(如网络设备、汽车和物联网解决方案)提供了通信基础。
### 其他 
| 工作组 | 标准名称/案例 | 
|---------|--------|
|cloud|Trusted Multi-Tenant Work Group Trust Assessment Framework|
|cloud|TCG Trusted Multi-Tenant Infrastructure Use Cases|
|cloud|Trusted Multi-Tenant Infrastructure Reference Framework|
|cloud|Cloud Computing and Security - A Natural Match|
|Infrastructure| TPM 2.0 Keys for Device Identity and Attestation|
|Infrastructure|TCG Component Class Registry|
|Infrastructure|TCG EK Credential Profile for TPM Family 2.0|
|Infrastructure|Canonical Event Log Format|
|Infrastructure|PCIe-based Component Class RegistryZ|
|Server|TCG Server Management Domain Firmware Profile Specification|
|virtualized Platform|Virtualized Trusted Platform Architecture Specification|
|Cyber Resilient Technologies|N/A|
|DICE|DICE Endorsement Architecture for Devices|
|DICE|DICE Attestation Architecture|
|DICE|DICE Layering Architecture|
|DICE|Symmetric Identity Based Device Attestation|
|DICE|DICE Certificate Profiles|
|Industrial|Industrial Internet Security Framework
|Industrial|Standards for Securing Industrial Equipment
|Industrial|Industrial Internet Security Framework
|Industrial|TNC IF-MAP Metadata for ICS Security|
|Industrial|TCG Guidance for Securing Industrial Control Systems Using TCG Technology|
|IoT|TCG Guidance for Secure Update of Software and Firmware on Embedded Systems|
|IoT|Architect's Guide IoT Security|
|Measurement AND Attestation Roots|TCG MARS API Specification Version 1, Revision 2|
|Measurement AND Attestation Roots|MARS Library Specification|
|Measurement AND Attestation Roots|MARS Use Cases and Considerations|
|Mobile|TCG Runtime Integrity Preservation in Mobile Devices|
|Mobile|TCG Trusted Network Communications for Mobile Platforms|
|Mobile|TCG TPM 2.0 Mobile Specification Implementation Guidance|
|Embedded Systems|N/A | 


