# Keylime最佳实践
## keylime概述

[Keylime](https://github.com/keylime/keylime) 是一个利用可信计算TPM 技术的开源可扩展信任系统。Keylime已经进入CNCF项目且被Redhat等多个主流发行版集成。Keylime 提供了一种端到端解决方案，用于为远程计算机引导基于硬件的加密信任、加密负载的配置以及运行时系统完整性监控。 它还为任何给定 PCR（平台配置寄存器）的远程证明提供了灵活的框架。 用户可以创建自己的自定义操作，当机器未通过其验证测量时将触发这些操作。

Keylime 的使命是让开发人员和用户能够轻松使用 TPM 技术，而无需深入了解 TPM 较低级别的操作。 在许多场景中，租户需要对于不受自己完全控制的机器的远程证明（例如混合云的消费者或位于不安全的、容易被篡改的物理位置的远程边缘/物联网设备）。

通过 CLI 应用程序和一组 RESTful APIs(包括http和https，其中https相关的RESTful APIs采用mTLS握手协议) 来执行和管理keylime。

Keylime 由三个主要组件组成；verifier、registrar和agent。
- verifer持续验证运行agent的计算机的完整性状态。
- registrar是在 Keylime 中注册的所有agent的数据库，并托管 TPM 供应商的公钥。
- agent部署在要监控的TPM机器上

![undefined](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/136439/1692101838904-ca289e62-b217-4c33-af50-33e260d96ee5.png) 

此外keylime还提供tenant工具便于用户远程管理agent。

## 龙蜥社区在keylime社区的工作与探索

龙蜥社区自其可信计算SIG成立以外，一直在关注可信计算业界进展和国际OSV厂商的可信计算方案。同时龙蜥社区在keylime社区积极贡献代码与适配，一共在rust-keylime和keylime两个仓库提交并合入17个patch，包括多个features、bugfixes和文档。详见[keylime release notes](https://github.com/keylime/keylime/releases)和[rust-keylime](https://github.com/keylime/rust-keylime/releases), 具体包括：
- features：集成龙蜥anolis以及下游阿里云Alibaba Cloud Linux OS的安装代码、集成阿里云vTPM EK证书、支持keylime安装时选择缺省的监听端口等。
- bugfixes: 修复measure boot时处理部分eventlog出错、修复rust-keylime跟keylime RESTful APIs版本和接口不一致、移除无用的代码等。
- 文档：修复安装文档和实践文档中多处命令错误等。

| 开源软件名称 | 总计commit数量 | 总计修改行数 |
| :- | --: | --: |
| [rust-keylime](https://github.com/keylime/rust-keylime) | 3 | -19/+20 | 
| [keylime](https://github.com/keylime/keylime) | 14 | -24/+156 | 

在完成keylime的适配和实践后，龙蜥社区也将自己的keylime经验写入到白皮书中。未来，龙蜥社区除了继续加强与keylime社区的沟通和贡献（参与keylime rust化）外，还将结合自己在国密/国产化/机密计算的积累围绕keylime开展一些国密、国产化、机密计算相关的工作，尽情期待。

## 龙蜥Anolis OS上keylime用途与实践

Keylime可以借助PCR或者Measure boot监控远程机器（Agent部署机器）的启动时的状态（完整性等）和借助IMA来监控运行时的完整性。开启对应的策略（policy）后，时刻轮询监控着对应agent的状态，如果发现异常则返回给verifier执行对应的操作（标记失败/停止轮询/打印错误等）。关于这部分的用法详见下文`Anolis OS上keylime高级功能实践`章节。

Keylime也可以通过RESTful APIs去监控/管理/查询 Keylime的agent、verifier以及registrar，便于用户以及运维人员有效的管理Keylime的各个组件以及验证远程机器的完整性等。此外keylime提供更加安全的mtls协议和基于Https的RESTful APIs。其中如果被正确执行，则RESTful APIs返回对应的信息且状态码为200，否则则为错误运行。关于这些APIs的用法详见下文`用Restful API去监控/管理Anolis OS上的各个keylime组件`章节。

### Anolis OS上keylime安装与配置、运行
#### 安装（以anolis 8.8为例）

keylime分为两个代码仓库：
- [keylime](https://github.com/keylime/keylime)：包含除了agent以外的其它keylime组件（verifier，registrar，tenant）, 下载后执行`cd keylime && ./installer.sh -i` 命令进行安装
- [rust-keylime](https://github.com/keylime/rust-keylime): 包含keylime的agent组件
	- 安装[tpm2-tss软件包](https://github.com/tpm2-software/tpm2-tss)(**如果keylime agent跟keylime 其它组件安装在一台机器，则这一步可以省略**)
	- 安装[tpm2-tools软件包](https://github.com/tpm2-software/tpm2-tools)(**如果keylime agent跟keylime 其它组件安装在一台机器，则这一步可以省略**)
	- 安装rust-keylime：可以根据以下命令在anolis（以anolis 8.8为例）上安装rust-keylime（keylime agent）

```shell
yum install -y libarchive-devel clang-devel rust cargo openssl-devel jq
git clone https://github.com/keylime/rust-keylime.git
cd rust-keylime
cargo build
make install
useradd keylime
mkdir -p /var/lib/keylime/cv_ca
# 将keylime verifier机器上的/var/lib/keylime/cv_ca/cacert.crt拷贝到agent
# 机器上/var/lib/keylime/cv_ca/目录下，以便于后续Agent侧https RESTful APIs的访问
chown -R keylime /var/lib/keylime
```

#### 配置
- verifier配置:`/etc/keylime/verifier.conf`为verifier的缺省配置。一般情况下不需要进行修改（当然您也可以根据您的需求进行修改）。
- registrar配置:`/etc/keylime/registrar.conf`为registrar的缺省配置。一般情况下不需要进行修改（当然您也可以根据您的需求进行修改）。
- agent配置:`/etc/keylime/agent.conf`为agent的缺省配置。当agent跟verifier、registrar部署在同一台机器时，不需要修改agent的配置；否则需要修改agent监听的IP、contact_ip(verifier和tenant用来连接的agent IP)、registrar的IP以便于正确注册和通信。
- tenant配置:`/etc/keylime/tenant.conf`为tenant的缺省配置。一般情况下不需要进行修改（当然您也可以根据您的需求进行修改）。

#### 运行

启动方式：
1. 以二进制方式启动verifier、registrar和agent：
2. 以systemd方式启动verifier、registrar（具体命令如下）

```shell
cd keylime
./services/installer.sh
systemctl start keylime_verifier
systemctl start keylime_registrar
systemctl start keylime_agent
```

### Anolis OS上keylime高级功能实践
#### 使用用户选择的PCR进行监控

该功能需要Agent侧有TPM，但因为TPM的PCR数量有限，扩展性不好。配置tpm_policy并用keylime_tenant工具进行添加，具体命令如下

```shell
keylime_tenant -v 121.43.60.253 -t 120.26.100.138 \
    --uuid d432fbb3-d2f1-4a97-9ef7-75bd81c00000 \
    --tpm_policy "{\"15\": [\"0000000000000000000000000000000000000000\", \
     \"0000000000000000000000000000000000000000000000000000000000000000\", \ 
\"00000000000000000000000000000000000000000000000000000000000000000000000000\
0000000000000000000000\"]}" \
    -c add --cert /var/lib/keylime/cv_ca
```

##### 监控

成功的case如下（agent时刻监控TPM PCRs的状态）

```shell
 DEBUG keylime_agent::quotes_handler  > Calling Integrity Quote with nonce: lCYwDxi2UkTIWM6Fk2aH, mask: 0x408000
 INFO  keylime_agent::quotes_handler  > GET integrity quote returning 200 response
 INFO  actix_web::middleware::logger  > GET 
                /v2.1/quotes/integrity?nonce=lCYwDxi2UkTIWM6Fk2aH&mask=0x408000&partial=1&ima_ml_entry=0 
                HTTP/1.1 from 121.43.60.253 result 200 (took 1229.894715 ms)
 INFO  keylime_agent                  > GET invoked from "121.43.60.253" with uri 
                /v2.1/quotes/integrity?nonce=WSn8mEpGLjN5I8mhHjPn&mask=0x408000&partial=1&ima_ml_entry=0
```

#### 使用Measured Boot

该功能
- 需要Agent有TPM以及使能IMA
- Keylime提供脚本create_mb_refstate生成对应的measured boot reference state和policy(根据/sys/kernel/security/tpm0/binary_bios_measurements里面的boot event log)
- Keylime提供keylime_tenant（--mb_refstate参数）添加对应的参考值和policy，然后启动Agent

![undefined](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/136439/1694077451486-1537c333-48af-4bed-a557-4b802f8fe48c.png) 

第一步，生成measure boot policy

```shell
cd keylime
./scripts/create_mb_refstate -i /sys/kernel/security/tpm0/binary_bios_measurements \
    ./measured_boot_reference_state.json
cat ./measured_boot_reference_state.json | jq .
```

用keylime_tenant工具进行添加mb_reference，对应的命令如下：

```shell
keylime_tenant -c update -t 120.26.100.138 -v 121.43.60.253 \ 
    -u d432fbb3-d2f1-4a97-9ef7-75bd81c00000 \ 
    --mb_refstate ./measured_boot_reference_state.json \
    --cert /var/lib/keylime/cv_ca
```

agent侧查看轮询结果:时刻轮询/监控着是否有异常。

```shell
 INFO  keylime_agent                  > GET invoked from "121.43.60.253" with uri 
                 /v2.1/quotes/integrity?nonce=EiRTFv9tgNBmwy6HwxAC&mask=0xfbff&partial=1&ima_ml_entry=0
 DEBUG keylime_agent::quotes_handler  > Calling Integrity Quote with nonce: EiRTFv9tgNBmwy6HwxAC, mask: 0xfbff
 INFO  keylime_agent::quotes_handler  > GET integrity quote returning 200 response
 INFO  actix_web::middleware::logger  > GET 
                /v2.1/quotes/integrity?nonce=EiRTFv9tgNBmwy6HwxAC&mask=0xfbff&partial=1&ima_ml_entry=0 
                HTTP/1.1 from 121.43.60.253 result 200 (took 1452.270707 ms)
```

#### Runtime Integrity Monitoring

该功能
- 需要Agent有TPM以及使能IMA
- Keylime提供脚本keylime_create_policy/create_runtime_policy.sh创建对应的runtime policy
- Keylime提供keylime_tenant（--runtime-policy参数）添加对应的runtime policy，然后启动Agent

使用keylime_create_policy工具来生成policy：

```shell
keylime_create_policy -m /sys/kernel/security/ima/ascii_runtime_measurements -o runtime_policy.json
cat runtime_policy.json | jq .
```

使用keylime_tenant来添加runtime_policy：

```shell
keylime_tenant -c update --uuid d432fbb3-d2f1-4a97-9ef7-75bd81c00000 \ 
      -t 120.26.100.138 -v 121.43.60.253 --runtime-policy /root/runtime_policy.json \
      --runtime-policy-name=tpm --cert /var/lib/keylime/cv_ca
```

查看runtime policy

```shell
## curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
     -k "https://127.0.0.1:8881/v2.1/allowlists/tpm"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "name": "tpm",
    "tpm_policy": null,
    "runtime_policy": ...
}
```

##### 监控IMA错误

从verifier的日志可以看到有一些没有进行IMA签名，无法验证，所有直接报错

```shell
2023-09-07 15:20:10.295 - keylime.tpm - INFO - Checking IMA measurement list on agent: 
   d432fbb3-d2f1-4a97-9ef7-75bd81c00000
2023-09-07 15:20:10.295 - keylime.ima - WARNING - Hashes for file boot_aggregate don't match
  1aa841ace294d93414158a2f070c92d078c464da0110269d3ad1e59367cdc285 not in
  ['fd2cf72bae331c6ba3db242e04f65ad5ca5c9da3f94dd5c78f5e56496e7cf0da']
2023-09-07 15:20:10.296 - keylime.ima - ERROR - IMA ERRORS: Some entries couldn't be validated. Number of 
  failures in modes: ImaSig 1.
2023-09-07 15:20:10.357 - keylime.verifier - WARNING - Agent d432fbb3-d2f1-4a97-9ef7-75bd81c00000 failed, 
  stopping polling
```

### 用Restful API去监控/管理Anolis OS上的各个keylime组件
#### registrar

使用registrar的RESTful APIs能够对agent进行注册、查询、删除、激活等操作。注意以下示例中的registrar IP需要根据实际IP进行修改。

##### GET /v2.1/agents/

用来获取注册的agents列表，具体命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \
     -k "https://127.0.0.1:8891/v2.1/agents"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "uuids": [
      "d432fbb3-d2f1-4a97-9ef7-75bd81c00000"
    ]
  }
}
```


##### GET /v2.1/agents/{agent_id:UUID}

获取对应agent的端口、IP、EK证书等信息, 命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \
     --cert /var/lib/keylime/cv_ca/client-cert.crt \
     -k "https://127.0.0.1:8891/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    ...
    "ip": "121.43.60.253",
    "port": 9002,
    "regcount": 1
  }
}
```

##### PUT /v2.1/agents/{agent_id:UUID}/activate

激活agent_id的agent，注意**这是一个http请求，不是https，如果用https会提示这个不是TLS接口**。命令如下:

```shell
# curl -k \
-X PUT "http://127.0.0.1:8890/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000/activate" \
      -H 'Content-Type: application/json' \ 
  -d '{"auth_tag":"166be150040c57b4e2c69ad7a5dd4c57059e5838a1df4715872f6e385e8ce1ed91"}' \
     | jq .
{
  "code": 200,
  "status": "Success",
  "results": {}
}
```

##### DELETE /v2.1/agents/{agent_id:UUID}

从registrar中移除ID为agent_id的agent，移除后再查看发现没有该agent了, 使用该命令

```shell
# curl -k \
-X PUT "http://127.0.0.1:8890/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000/activate" \
      -H 'Content-Type: application/json' \ 
  -d '{"auth_tag":"166be150040c57b4e2c69ad7a5dd4c57059e5838a1df4715872f6e385e8ce1ed91"}' \
     | jq .
{
  "code": 200,
  "status": "Success",
  "results": {}
}
# curl --key /var/lib/keylime/cv_ca/client-private.pem \
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
     -k "https://127.0.0.1:8891/v2.1/agents"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "uuids": []
  }
}
```

##### POST /v2.1/agents/{agent_id:UUID}

注册agent_id的agent到registrar.**这是一个http不是https的请求。** 注册及注册后的查询命令如下


```shell
# curl -X POST  "http://127.0.0.1:8890/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000" 
  -H 'Content-Type: application/json' \ 
  -d '{ "ekcert": "MIIE3DCCA8SgAwIBAgIBBDANBgkqhkiG9w0BAQsFADBvMQswCQYDVQQGEwJDTjEPMA0GA1UECgwGQWxpeXVuMTIwMA" \ 
 "aik_tpm": "ARgAAQALAAUAcgAAABAAFAALCAAAAAAAAQC7R7SiAAExqqCZJ60cTJXxcYMCRsctsh96vX/f2T31DMrB6SnCMV9euHlMUCUs" \ 
"ip": "127.0.0.1","port": 9002}' | jq .
{
  "code": 200,
  "status": "Success",
  ...
}
# curl --key /var/lib/keylime/cv_ca/client-private.pem --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
   -k "https://127.0.0.1:8891/v2.1/agents"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "uuids": [
      "d432fbb3-d2f1-4a97-9ef7-75bd81c00000"
    ]
  }
}
```

#### verifier

##### GET /v2.1/agents/{agent_id:UUID}

从CV中获取agent `agent_id`的状态。具体命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
     -k "https://127.0.0.1:8881/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    ...
    "hash_alg": "sha256",
    "enc_alg": "rsa",
    "sign_alg": "rsassa",
    "verifier_id": "default",
    "verifier_ip": "121.43.60.253",
    "verifier_port": 8881,
    "severity_level": 6,
  }
}
```

##### PUT /v2.1/agents/{agent_id:UUID}/stop

停止对 `agent_id` 的 cv 轮询，但不要删除（对于已经启动的 agent_id）。具体命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \ 
   -X PUT "https://127.0.0.1:8881/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000/stop" \
    | jq .
{
  "code": 200,
  "status": "Success",
  "results": {}
}
```

##### DELETE /v2.1/agents/{agent_id:UUID}

删除 agent_id实例。删除包括删除后的查看命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
  -k -X DELETE "https://127.0.0.1:8881/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000" \
     | jq .
{
  "code": 200,
  "status": "Success",
  "results": {}
}
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
     -k "https://121.0.0.1:8881/v2.1/agents/d432fbb3-d2f1-4a97-9ef7-75bd81c00000"  | jq .
{
  "code": 404,
  "status": "agent id not found",
  "results": {}
}
```

##### GET /v2.1/allowlists/{runtime_policy_name:string}

从 CV 中检索命名的运行时策略 runtime_policy_name。比如tpm的policy创建了，可以通过以下命令查看

```shell
curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \
     "https://127.0.0.1:8881/v2.1/allowlists/tpm"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "name": "tpm",
    "tpm_policy": null,
    "runtime_policy": ...
  }
}
```

对于没有创建的test policy，查询是没有的

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \ 
     "https://127.0.0.1:8881/v2.1/allowlists/test"  | jq .
{
  "code": 404,
  "status": "Runtime policy test not found",
  "results": {}
}
```

##### DELETE /v2.1/allowlist/{runtime_policy_name:string}

删除 IMA policy `runtime_policy_name`. 比如删除已有的`tpm` policy，然后再测试，发现该policy没了

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \
     -k "https://127.0.0.1:8881/v2.1/allowlists/tpm"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "name": "tpm",
    "tpm_policy": null,
    "runtime_policy": ...
  }
}
# curl --key /var/lib/keylime/cv_ca/client-private.pem \
     --cert /var/lib/keylime/cv_ca/client-cert.crt -X DELETE \
     -k "https://127.0.0.1:8881/v2.1/allowlists/tpm"  | jq .
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt \ 
     -k "https://127.0.0.1:8881/v2.1/allowlists/test"  | jq .
{
  "code": 404,
  "status": "Runtime policy test not found",
  "results": {}
}
```

而删除一个不存在的policy test，会报错

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \
     --cert /var/lib/keylime/cv_ca/client-cert.crt -X DELETE \
     -k "https://127.0.0.1:8881/v2.1/allowlists/test"  | jq .
{
  "code": 404,
  "status": "Runtime policy test not found",
  "results": {}
}
```

#### agent

##### GET /version

获取agent支持的API版本。对应的命令如下:

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \
     "https://127.0.0.1:9002/version" | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "supported_version": "2.1"
  }
}
```

##### GET /v2.1/keys/pubkey

获取agent的公钥. 对应的命令如下:

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \ 
     "https://127.0.0.1:9002/v2.1/keys/pubkey"  | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
	"pubkey": ...
  }
}
```

##### GET /v2.1/quotes/identity

从节点获取identity quote, 对应的命令如下:

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \ 
     "https://127.0.0.1:9002/v2.1/quotes/identity?nonce=1234567890ABCDEFHIJ" \
     | jq .
{
  "code": 200,
  "status": "Success",
  "results": {
    "quote": ...
    "hash_alg": "sha256",
    "enc_alg": "rsa",
    "sign_alg": "rsassa",
    "pubkey": ...
  }
}
```

##### GET /v2.1/quotes/integrity

从节点获取integrity quote，具体命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem  \ 
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \ 
"https://127.0.0.1:9002/v2.1/quotes/integrity?nonce=1234567890&mask=0x10401&partial=0" 
{
  "code": 200,
  "status": "Success",
  "results": {
    "quote": ...
    "hash_alg": "sha256",
    "enc_alg": "rsa",
    "sign_alg": "rsassa",
    "pubkey": ...
    "ima_measurement_list": ...
    "mb_measurement_list": ...
    "ima_measurement_list_entry": 0
  }
}
```

##### GET /v2.1/keys/verify

获取bootstrap key的验证, 对应的命令如下：

```shell
# curl --key /var/lib/keylime/cv_ca/client-private.pem \
     --cert /var/lib/keylime/cv_ca/client-cert.crt -k \
    "https://127.0.0.1:9002/v2.1/keys/verify?challenge=1234567890ABCDEFHIJ"  | jq .
{
  "code": 400,
  "status": "Bootstrap key not yet available.",
  "results": {}
}
```
