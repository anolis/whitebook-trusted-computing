# 海光平台可信计算最佳实践
## 1. 可信计算整体架构
### 1.1. 概述

 图 1 可信计算架构

 ![](images/hygon/tc-1.png)

CPU利用内置安全处理器对可信计算做了相关支持与拓展，在CPU内部实现了TPM2.0模块（Trusted Platform Module，国际标准）、TCM2.0模块（Trusted Crypto Module，中国传统标准）、TPCM模块（Trusted Platform Control Module，中国新兴标准）、TDM模块（Trusted Dynamic Measuring, 私有）及TSB模块（Trusted Secure Boot，私有）五大可信功能模块。利用上述功能模块可以实现可信计算的核心功能，如可信启动、动态度量、可信存储、可信报告等。
### 1.2. 可信计算支持
#### 1.2.1. TPM2.0

基于安全处理器以固件的形式实现了TPM2.0设备，命令接口符合TPM2.0规范。同时，利用内置的密码运算硬件加速引擎CCP实现所有密码运算相关操作，克服传统固件实现TPM的不足，提高了系统性能，硬件加速引擎全面支持商密标准。

#### 1.2.2. TCM2.0

TCM是国内传统的可信计算标准，有着广泛的应用。2020年国密局升级了TCM标准，推出了TCM2.0规范《GM/T 0012-2020 可信计算 可信密码模块接口规范》。TCM2.0功能与接口的定义基于TPM2.0，与TPM2.0具有较强的兼容性，TCM2.0只支持商密标准。与TPM2.0一样，TCM2.0基于CPU安全处理器以固件的形式实现，固件以BIOS PI的形式发布给OEM厂商，因此使用TCM2.0需与OEM厂商确认BIOS已使能该功能支持。

#### 1.2.3. TPCM

TPCM（Trusted Platform Control Module）是国内新兴的可信计算技术，是中国可信计算3.0的底层芯片标准，实现可信计算的信任根，由中关村可信计算联盟制定。和TPM/TCM相比，TPCM增加了对系统主动监视和控制的功能，可以实现系统启动时的主动启动度量，及程序运行时的动态度量和监控，进一步增强系统的安全性。

#### 1.2.4. TDM

TDM（Trusted Dynamic Measurement）为基于安全处理器实现的轻量级动态度量，是特有功能。通过TDM可以实现对设定内存目标进行持续的周期性度量，及时发现程序异常，保护程序运行时安全；同时TDM通过独有的双重授权保护方式确保非授权用户无法篡改TDM内的度量任务设定，极大的增强了模块的安全性。

#### 1.2.5. TSB

平台固件BIOS的完整性验证是启动信任链可信的关键和基础，传统的CPU由于缺少专门硬件支持无法验证BIOS的完整性。TSB（Trusted Secure Boot）是除了上述TPCM/TPM/TCM之外另一个独立的由CPU硬件验证平台固件BIOS完整性的功能，验证基于数字签名。CPU上电或重启时，TSB首先验证颁发的OEM公钥证书，再用OEM公钥证书中的公钥验证BIOS固件的OEM签名，验证通过后才运行BIOS固件，从而保证BIOS固件的安全以及后续整个启动信任链的源头安全。

## 2. 软件编译说明
### 2.1. 安装说明

(1)安装OS为Anolis系列镜像时，功能测试不需要额外安装软件包，可跳过软件编译说明部分直接进行功能测试即可；

(2)安装OS镜像为开源版本时，功能测试需要另外安装测试软件包或模块，具体安装说明如下。

### 2.2. tpm2

**tpm2功能测试需要安装相应软件栈，安装包(RPM)的生成及安装脚本已上传龙蜥社区开源仓库，使用如下：**

```
$ git clone https://gitee.com/anolis/hygon-devkit.git
$ cd hygon-devkit/tpm/pkg/tpm-1.0.0-20230331/
$ ./install.sh
```
编译并安装完成后按照下文TPM功能测试文档的步骤测试即可。

注：tpm2功能测试还需要安装grub，具体安装步骤请参考本页面grub安装相关说明。

### 2.3. tdm

**tdm驱动已集成到Anolis OS 5.10内核。测试TDM功能时还需要相应的测试module，已上传龙蜥社区仓库，安装步骤如下：**
```
$ git clone https://gitee.com/anolis/hygon-devkit.git
$ cd hygon-devkit/tdm/pkg/tdm-1.0.0-20230316/
$ make LOCAL_KERDIR=/lib/modules/`uname -r`/build
```
编译通过后可在当前目录下生成tdm-verify.ko，将该测试module拷贝至测试目录下，再按照下文TDM功能测试文档的步骤测试即可。

### 2.4. grub

完整的可信启动信任链包含grub度量OS内核，支持tpm2的grub版本需2.04或以上，同时需将grub tpm模块安装进grub内核。以Anolis OS 8.8为例：

**1）若当前系统grub版本（“grub2-install --version”查看）已满足要求，则只需将tpm模块安装进grub内核，具体步骤如下：**
```
$ sudo mkdir /boot/efi/bak
$ sudo cp -fr /boot/efi/EFI /boot/efi/bak/
$ sudo cp -fr /boot/efi/boot /boot/efi/bak/
$ sudo grub2-install --efi-directory=/boot/efi --bootloader-id=anolis --boot-directory=/boot/efi/boot \ 
--target=x86_64-efi --modules=tpm
$ sudo grub2-mkconfig -o /boot/efi/boot/grub/grub.cfg
```
注：

- 上述/boot/efi/bak相关步骤（步骤1~3）是防止安装grub出错导致系统无法启动进行的备份操作，下同；

- 不同系统的grub安装命令可能会不同，比如centos系统，一般使用grub2-install、grub2-mkconfig等命令，需要注意区分；

- grub install时参数bootloader-id可根据系统不同进行替换，当前系统可通过/boot/efi/EFI目录查看；

- 执行完上述步骤后需要重启。

**2）若当前系统grub版本不满足要求，则重新编译安装grub，具体步骤如下：**
```
$ wget https://ftp.gnu.org/gnu/grub/grub-2.04.tar.gz
$ tar -zxvf grub-2.04.tar.gz
$ cd grub-2.04/
$ ./bootstrap
$ ./configure --host=x86_64-linux --target=x86_64 --with-platform=efi
$ make
$ sudo make install
$ sudo cp /etc/default/grub /usr/local/etc/default/
$ sudo mkdir /boot/efi/bak
$ sudo cp -fr /boot/efi/EFI /boot/efi/bak/
$ sudo cp -fr /boot/efi/boot /boot/efi/bak/
$ sudo /usr/local/sbin/grub-install --efi-directory=/boot/efi --bootloader-id=anolis \ 
--boot-directory=/boot/efi/boot --target=x86_64-efi --modules=tpm
$ sudo /usr/local/sbin/grub-mkconfig -o /boot/efi/boot/grub/grub.cfg
```
注：

- bootstrap在发布版Grub里可能没有，没有则不需要运行；
- 拷贝/etc/default/grub配置文件到/usr/local/etc/default目录时，如果本地不存在该目录，请创建；

## 3. TPM2.0功能测试
### 3.1. 概述

该章节演示了在一台安装Anolis OS的服务器上测试TPM2.0功能的完整步骤。演示的主要TPM应用包括：BIOS/GRUB/Linux启动度量，商密测试。

注：

- 发行的Anolis OS已包含TPM2.0功能在内的安全功能适配支持，安装后用户可以直接测试使用，开源镜像的软件栈安装步骤等请参考第一章节。
- 支持tpm2商密的tss和tools最低版本分别是2.3.2-4.0.2和4.1.1-5.0.3，如果不满足请更新版本。

表1 配置要求

| 配置         | 要求                   |
|--------------|-----------------------|
| CPU          | 2号、3号               |
| BIOS         | PI 版本为2.1.0.3或以上，支持TPM2.0 |
| GRUB         | 2.04                  |
| OpenSSL      | openssl-1.1.1k        |
| KERNEL       | 5.10.134              |
| TPM2-tss     | tpm2-tss-2.3.2        |
| TPM2-abrmd   | tpm2-abrmd-2.3.3      |
| TPM2-tools   | tpm2-tools-4.1.1      |

本次测试安装的ISO镜像名称为AnolisOS-8.8-x86_64-dvd.iso ，具体名称请以发布为准。

### 3.2. BIOS安装及TPM设置

进入BIOS设置，在TCG Trusted Computing选项下关于TPM的默认设置如下图1所示，度量使用SM3算法，三个Hierarchy(Platform, Storage, Endorsement)全部使能，如果没特殊需求，使用默认设置即可。
BIOS中开启TPM的设置在不同的BIOS下可能会有差异，具体请咨询相应的BIOS厂商。以下设置仅供参考：

 图 2 BIOS设置

 ![](images/hygon/tpm2-1.png)

### 3.3. 依赖软件包

tpm2.0依赖的软件栈或者安装包等都已经集成进ISO，如果当前环境缺少对应安装包，可直接使用yum安装。

需要注意的是tpm驱动默认以ko模块的方式集成到kernel中，如果需要测试IMA功能，则需要将TPM驱动编译进内核，不能以模块的方式加载。如果不需要使用IMA则可以忽略。

### 3.4. 配置

安装相应安装包后，在测试tpm2功能之前，需要设置如下配置并启动tpm2-abrmd服务：
```
$ sudo useradd --system --user-group tss
$ sudo udevadm control --reload-rules && udevadm trigger
$ sudo pkill -HUP dbus-daemon
$ sudo systemctl daemon-reload
$ sudo ldconfig
$ sudo systemctl enable tpm2-abrmd
$ sudo chown tss:tss /dev/tpm0
$ sudo service tpm2-abrmd start
$ systemctl status tpm2-abrmd.service
```

### 3.5. BIOS/Grub/Linux内核启动度量

启动度量对TPM PCR的使用情况如表2：

表2 TPM PCR的使用情况

| PCR编号      | 度量目标                |
|--------------|-----------------------|
| 0            | BIOS，包括作为度量根的初始代码  |
| 1            | BIOS平台配置                 |
| 2            | OPTION ROM代码              |
| 3            | OPTION ROM配置及数据         |
| 4            | IPL/Grub代码                |
| 5            | IPL/Grub配置及数据           |
| 6            | STATE_TRANSITION           |
| 7            | 平台厂商相关控制              |
| 8            | GRUB执行的命令字符串，包括配置文件内命令和命令行输入 |
| 9            | GRUB 打开的文件，包括配置文件，efi子模块，Linux内核，Initrd等 |        
| 10           | Linux IMA使用               |

系统起来后使用工具tpm2_pcrread读取PCR。根据PCR使用分配，在不改变BIOS/Grub配置的情况下，每次系统启动后PCR 0~9读值应保持不变，如下。
```
[hygon@localhost ~]$ tpm2_pcrread sm3_256:0,1,2,3,4,5,6,7,8,9,10,11
sm3_256:
  0 : 0x5D25A693796A9D6060834A9FB0AF416E9C9FB4D47A22326BBC45686300B471A3
  1 : 0xFAC21DA05E1F8467972D6ABAF2CBED26FBB81B20A26A2751D32798EE574A7B1F
  2 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  3 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  4 : 0xA2381A4E30198BED49FB10A3D86274930B10D0EE788ED1121D1B83CF814362C9
  5 : 0xC71BED60766F8B89F6296C076F88E702B0E0474ACB8019AC8B8DB52E66E739ED
  6 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  7 : 0x2304AF3530A51BC03051BA7D3A2BB7B462120DF1B1D13BB55FA0B565831C19F4
  8 : 0xDEA0758622845043428A74802AABB23B53941CD216BA934FBFB5AF4D69FD7905
  9 : 0x2A12748335078EF34ABB0803C293AD390C2D9AB8B6D5FA9086E6A1ED4CD706FD
  10: 0x0000000000000000000000000000000000000000000000000000000000000000
  11: 0x0000000000000000000000000000000000000000000000000000000000000000
```

### 3.6. 商密支持的测试
#### 3.6.1. 商密测试脚本

包含商密测试的脚本已入库龙蜥仓库，具体见tpm2-tools仓库a8分支，patch名称：0001-add-gm-test-case-for-all-commands.patch

#### 3.6.2. 运行测试

命令正确执行的结果如下:
```
[hygon@localhost tests_gm]$ ./test.sh 
test_tpm2_activatecredential.sh ... PASSED
test_tpm2_attest.sh ... PASSED
test_tpm2_changeauth.sh ... PASSED
test_tpm2_clock.sh ... PASSED
test_tpm2_encryptdecrypt.sh ... PASSED
test_tpm2_hash.sh ... PASSED
test_tpm2_nv.sh ... PASSED
test_tpm2_pcr.sh ... PASSED
test_tpm2_policy.sh ... PASSED
test_tpm2_random.sh ... PASSED
test_tpm2_selftest.sh ... PASSED
test_tpm2_sign.sh ... PASSED
Tests passed: 12
Tests Failed: 0
```

#### 3.6.3. 测试脚本说明

- 每个以test开头的脚本是测试TPM的一类脚本,比如测试TPM policy相关命令的脚本名字为test_tpm2_policy.sh
- 脚本中测试命令都是测试商密的命令,TPM算法解析是object:scheme:symdetail 格式的字符串，比如tpm2_createprimary -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c /tmp/context,字符串eccsm2:null:sm4128cfb就是在上面格式的基础上添加商密相关的字符串sm2，sm4128cfb以支持商密。在某些命令中需要增加额外的参数支持商密选择，同时保持兼容以前的密码算法，具体命令可参考脚本中tpm2_certify，tpm2_createak，tpm2_createek，tpm2_createprimary，tpm2_loadexternal，tpm2_nvdefine，tpm2_quote，tpm2_startauthsession的使用。

## 4. TDM功能测试
### 4.1. 概述

如下章节演示了在一台安装Anolis OS的服务器上使用TDM功能的步骤，演示的主要TDM应用包括：TDM动态保护任务创建运行销毁流程，TDM动态保护任务更新操作流程，TDM动态保护防字典攻击流程，TDM基于虚拟地址创建运行销毁度量任务流程，TDM度量任务异常触发机器挂机流程，TDM证书获取与证书链验证流程，TDM度量报告获取与验证流程，TDM VPCR获取与重放VPCR值验证流程。

注：1）发行的Anolis OS已包含TDM功能在内的安全功能适配支持，安装后用户可以直接测试使用，TDM测试module的获取请参照1.3章节。

表1 配置要求

| 配置         | 要求                   |
|--------------|-----------------------|
| CPU          | 2号、3号               |
| BIOS         | PI 版本为2.1.0.3或以上，支持TDM |
| KERNEL       | 5.10.134              |
| hag          | 1667或以上             |
| TDM固件       | 1.4或以上              |
| TDM驱动       | 0.7或以上              |
| TPM2-tools   | tpm2-tools-4.1.1      |

本次测试安装的ISO镜像名称为AnolisOS-8.8-x86_64-dvd.iso ，具体名称请以发布为准。

### 4.2. TDM测试环境配置
#### 4.2.1. TDM驱动加载

ISO镜像中的内核已包含TDM驱动，机器启动完成后dmesg输出下面log则表示TDM驱动已正常加载。
```
tdm: Thread started for measurement exception handler dispatching...
tdm: TDM driver loaded successfully!
```

#### 4.2.2. 使用hag查看TDM设备信息
注：

- hag工具可在龙蜥社区获取，hygon-devkit仓库bin目录下；
- 可将该工具拷贝至测试目录，并添加到PATH使用；

输出如下所示，可以看到成功获取TDM设备信息
```
[root@localhost hygon]# hag tdm show_tdm_device
###########################TDM_SHOW_DEVICE##################################
api_major         : 1
api_minor         : 4
buildId           : 1882
task_max          : 100
range_max_per_task: 128
show tdm device successful.
show_tdm_device command success!

[tdm] Command successful!
```

#### 4.2.3. tpm2_tools配置

Tpm2_tools工具主要用来读取PCR的值，VPCR需要使用到fTPM，tpm2_tools工具已默认集成到ISO镜像，不需额外配置。

### 4.3. TDM动态保护任务创建运行销毁流程

该场景主要测试《PSP动态度量接口规范》中的第七部分“度量任务创建及运行”，实现了度量任务的创建、注册异常回调、启动度量、停止度量、销毁度量任务的完成过程，在验证各个命令正常的情况下同时测试了正常启动度量的流程。

参考用例场景0测试主流程逻辑如下图2所示，详细细节可阅读参考用例tdm_verify.c的代码逻辑：

 图 3 TDM场景0测试流程

 ![](images/hygon/tdm-1.png)

test_scene表示测试场景，0表示正常创建场景测试，1表示更新操作场景，2表示防字典攻击场景，3表示虚拟地址任务场景

安装本次测试模块
```
$ sudo insmod tdm-verify.ko test_scene=0
```
移除本次测试模块（相较于以前版本增加了证书链验证与度量报告支持，后续测试需要使用该模块的度量任务，因此在移除模块时才释放度量任务）
```
$ sudo rmmod tdm-verify.ko
```
查看Log信息
```
$ dmesg
```
输出下面的log则表示TDM测试场景0测试成功，若出现error字样，表示该场景测试失败。
```
               –-----Victim module: has 3 blocks of data measured by PSP--------test_scene:0
[23048.936285] Call psp_create_measure_task to request measuring service.
[23048.936286] tdm: TDM: Can't get max_pfn, skip physical address check
[23048.936964] Call psp_register_measure_exception_handler to register measuring exception function for task: 0
[23048.937059] Call psp_startstop_measure_task to start measuring for task: 0
[23048.980357] Call psp_create_measure_task to request measuring service.
[23048.980358] tdm: TDM: Can't get max_pfn, skip physical address check
[23048.983682] Call psp_register_measure_exception_handler to register measuring exception function for task: 1
[23048.986755] Call psp_startstop_measure_task to start measuring for task: 1
[23049.035503] Call psp_create_measure_task to request measuring service.
[23049.035505] tdm: TDM: Can't get max_pfn, skip physical address check
[23049.039294] Call psp_register_measure_exception_handler to register measuring exception function for task: 2
[23049.042308] Call psp_startstop_measure_task to start measuring for task: 2
[23059.357713] Call psp_startstop_measure_task to stop measuring for task: 0
[23059.402299] Call psp_destroy_measure_task to destroy measuring service for task: 0
[23059.406240] Call psp_startstop_measure_task to stop measuring for task: 1
[23059.451298] Call psp_destroy_measure_task to destroy measuring service for task: 1
[23059.457734] Call psp_startstop_measure_task to stop measuring for task: 2
[23059.502293] Call psp_destroy_measure_task to destroy measuring service for task: 2
[23059.502470] 
               –----------------------end----------------------------
```

### 4.4. TDM动态保护任务更新操作流程

该场景主要测试《PSP动态度量接口规范》中的第八部分“度量任务基准值更新”，实现了对正在正常运行的度量任务的基准值更新，通过更新后基准值与实际度量结果不匹配而出发异常回调，同时验证了更新命令与异常触发回调的流程。

参考用例场景1测试主流程如下图3所示：

 图 4 TDM场景1测试流程

 ![](images/hygon/tdm-2.png)

test_scene表示测试场景，0表示正常创建场景测试，1表示更新操作场景，2表示防字典攻击场景，3表示虚拟地址任务场景

安装本次测试模块
```
$ sudo insmod tdm-verify.ko test_scene=1
```
移除本次测试模块（相较于以前版本增加了证书链验证与度量报告支持，后续测试需要使用该模块的度量任务，因此在移除模块时才释放度量任务）
```
$ sudo rmmod tdm-verify.ko
```
查看Log信息
```
$ dmesg
```
输出下面的log则表示TDM测试场景1测试成功，默认demo测试触发三次异常度量，在内核空间将有三个对应任务id的异常函数被触发。
```
               –-----Victim module: has 3 blocks of data measured by PSP--------test_scene:1
[23074.069910] Call psp_create_measure_task to request measuring service.
[23074.069911] tdm: TDM: Can't get max_pfn, skip physical address check
[23074.070566] Call psp_register_measure_exception_handler to register measuring exception function for task: 3
[23074.070661] Call psp_startstop_measure_task to start measuring for task: 3
[23074.112871] Call psp_create_measure_task to request measuring service.
[23074.112872] tdm: TDM: Can't get max_pfn, skip physical address check
[23074.116483] Call psp_register_measure_exception_handler to register measuring exception function for task: 4
[23074.119492] Call psp_startstop_measure_task to start measuring for task: 4
[23074.166990] Call psp_create_measure_task to request measuring service.
[23074.166992] tdm: TDM: Can't get max_pfn, skip physical address check
[23074.170536] Call psp_register_measure_exception_handler to register measuring exception function for task: 5
[23074.173597] Call psp_startstop_measure_task to start measuring for task: 5
[23074.331699] Call psp_startstop_measure_task to stop measuring for task: 3
[23074.375645] Call psp_update_measure_task to update measuring for task: 3
[23074.377384] Call psp_startstop_measure_task to start measuring for task: 3
[23074.394873] tdm: -----Measurement exception handler dispatching thread------
[23074.394874] tdm: Measurement exception received for task 3
[23074.394875] tdm: Step1: Query PSP for task 3 status to confirm the error.
[23074.394876] tdm: Step2: Error confirmed, CALL measurement exception handler.
[23074.394926] Call psp_startstop_measure_task to stop measuring for task: 4
[23074.401037] tdm: Error detected for task 3, action TODO!
[23074.401039] tdm: ----Measurement exception handler----
[23074.401040] ALARM!
[23074.401040] Task:3, corruption detected!
[23074.401041] Please check if it's intended, or your machine may be on danger!
[23074.401041] tdm: Exit measurement exception handler.
[23074.439650] Call psp_update_measure_task to update measuring for task: 4
[23074.440293] Call psp_startstop_measure_task to start measuring for task: 4
[23074.454929] tdm: -----Measurement exception handler dispatching thread------
[23074.454931] tdm: Measurement exception received for task 4
[23074.454931] tdm: Step1: Query PSP for task 4 status to confirm the error.
[23074.454932] tdm: Step2: Error confirmed, CALL measurement exception handler.
[23074.454953] Call psp_startstop_measure_task to stop measuring for task: 5
[23074.458098] tdm: Error detected for task 4, action TODO!
[23074.458100] tdm: ----Measurement exception handler----
[23074.458100] ALARM!
[23074.458101] Task:4, corruption detected!
[23074.458101] Please check if it's intended, or your machine may be on danger!
[23074.458102] tdm: Exit measurement exception handler.
[23074.502635] Call psp_update_measure_task to update measuring for task: 5
[23074.502728] Call psp_startstop_measure_task to start measuring for task: 5
[23074.517418] tdm: -----Measurement exception handler dispatching thread------
[23074.517420] tdm: Measurement exception received for task 5
[23074.517420] tdm: Step1: Query PSP for task 5 status to confirm the error.
[23074.517421] tdm: Step2: Error confirmed, CALL measurement exception handler.
[23074.517477] tdm: Error detected for task 5, action TODO!
[23074.517478] tdm: ----Measurement exception handler----
[23074.517478] ALARM!
[23074.517479] Task:5, corruption detected!
[23074.517479] Please check if it's intended, or your machine may be on danger!
[23074.517479] tdm: Exit measurement exception handler.
[23076.730176] Call psp_destroy_measure_task to destroy measuring service for task: 3
[23076.730346] Call psp_destroy_measure_task to destroy measuring service for task: 4
[23076.730486] Call psp_destroy_measure_task to destroy measuring service for task: 5
[23076.730651] 
               –----------------------end----------------------------
```

### 4.5. TDM动态保护防字典攻击流程

该场景主要测试《PSP动态度量接口规范》中的第四部分“防字典攻击”，实现了尝试对正在运行度量任务授权码暴力破解，进而触发DA保护启动的逻辑。

参考用例场景2测试主流程如下图4所示，流程中尝试两次错误的授权，将DA保护时间延长至2^(2-1)=2秒，并在DA保护的2秒区间内使用正确的授权码执行命令，由于DA保护，将返回在DA保护的状态，2秒保护结束后，使用正确的授权码授权执行命令，将恢复正常，详细逻辑可以参考用例代码。

图 5 TDM场景2测试流程

 ![](images/hygon/tdm-3.png)

test_scene表示测试场景，0表示正常创建场景测试，1表示更新操作场景，2表示防字典攻击场景，3表示虚拟地址任务场景

安装本次测试模块
```
$ sudo insmod tdm-verify.ko test_scene=2
```
移除本次测试模块（相较于以前版本增加了证书链验证与度量报告支持，后续测试需要使用该模块的度量任务，因此在移除模块时才释放度量任务）
```
$ sudo rmmod tdm-verify.ko
```
查看Log信息
```
$ dmesg
```
输出下面的log则表示TDM测试场景2测试成功，默认demo测试三个任务的DA攻击 ，在DA保护期间发送命令将拒绝服务返回错误。
```
               –-----Victim module: has 3 blocks of data measured by PSP--------test_scene:2
[23090.387377] Call psp_create_measure_task to request measuring service.
[23090.387378] tdm: TDM: Can't get max_pfn, skip physical address check
[23090.388051] Call psp_register_measure_exception_handler to register measuring exception function for task: 6
[23090.388126] Call psp_startstop_measure_task to start measuring for task: 6
[23090.434179] Call psp_create_measure_task to request measuring service.
[23090.434181] tdm: TDM: Can't get max_pfn, skip physical address check
[23090.437755] Call psp_register_measure_exception_handler to register measuring exception function for task: 7
[23090.440727] Call psp_startstop_measure_task to start measuring for task: 7
[23090.488312] Call psp_create_measure_task to request measuring service.
[23090.488314] tdm: TDM: Can't get max_pfn, skip physical address check
[23090.491870] Call psp_register_measure_exception_handler to register measuring exception function for task: 8
[23090.494848] Call psp_startstop_measure_task to start measuring for task: 8
[23090.541148] Call psp_startstop_measure_task in scene2 to stop measuring for task: 6
[23090.551624] tdm: psp_startstop_measure_task exception error: 0x2
[23091.574741] tdm: psp_startstop_measure_task exception error: 0x2
[23091.577725] tdm: psp_startstop_measure_task exception error: 0x5
[23091.580672] tdm: psp_startstop_measure_task exception error: 0x5
[23093.614989] Call psp_startstop_measure_task in scene2 to stop measuring for task: 7
[23093.626034] tdm: psp_startstop_measure_task exception error: 0x2
[23094.646682] tdm: psp_startstop_measure_task exception error: 0x2
[23094.649850] tdm: psp_startstop_measure_task exception error: 0x5
[23094.649913] tdm: psp_startstop_measure_task exception error: 0x5
[23096.688192] Call psp_startstop_measure_task in scene2 to stop measuring for task: 8
[23096.695727] tdm: psp_startstop_measure_task exception error: 0x2
[23097.720379] tdm: psp_startstop_measure_task exception error: 0x2
[23097.720457] tdm: psp_startstop_measure_task exception error: 0x5
[23097.723599] tdm: psp_startstop_measure_task exception error: 0x5
[23103.104351] Call psp_destroy_measure_task to destroy measuring service for task: 6
[23103.104526] Call psp_destroy_measure_task to destroy measuring service for task: 7
[23103.104664] Call psp_destroy_measure_task to destroy measuring service for task: 8
[23103.104823] 
               –----------------------end----------------------------
```

### 4.6. TDM度量任务通过虚拟地址创建运行销毁流程（1.3固件版本后支持）

参考用例场景3测试主流程逻辑与场景0基本相同，主要调整为度量任务创建改用虚拟地址进行创建，详细细节可阅读参考用例tdm_verify.c的代码逻辑。

test_scene表示测试场景，0表示正常创建场景测试，1表示更新操作场景，2表示防字典攻击场景，3表示虚拟地址任务场景

安装本次测试模块
```
$ sudo insmod tdm-verify.ko test_scene=3
```
移除本次测试模块（相较于以前版本增加了证书链验证与度量报告支持，后续测试需要使用该模块的度量任务，因此在移除模块时才释放度量任务）
```
$ sudo rmmod tdm-verify.ko
```
查看Log信息
```
$ dmesg
```
输出下面的log则表示TDM测试场景3测试成功.
```
               –-----Victim module: has 3 blocks of data measured by PSP--------test_scene:3
[23115.105615] Call psp_create_measure_task to request measuring service.
[23115.106269] Call psp_register_measure_exception_handler to register measuring exception function for task: 9
[23115.106351] Call psp_startstop_measure_task to start measuring for task: 9
[23115.151078] Call psp_create_measure_task to request measuring service.
[23115.154563] Call psp_register_measure_exception_handler to register measuring exception function for task: 10
[23115.157550] Call psp_startstop_measure_task to start measuring for task: 10
[23115.204366] Call psp_create_measure_task to request measuring service.
[23115.207533] Call psp_register_measure_exception_handler to register measuring exception function for task: 11
[23115.210041] Call psp_startstop_measure_task to start measuring for task: 11
[23117.109442] Call psp_startstop_measure_task to stop measuring for task: 9
[23117.153770] Call psp_destroy_measure_task to destroy measuring service for task: 9
[23117.157152] Call psp_startstop_measure_task to stop measuring for task: 10
[23117.200769] Call psp_destroy_measure_task to destroy measuring service for task: 10
[23117.204428] Call psp_startstop_measure_task to stop measuring for task: 11
[23117.247762] Call psp_destroy_measure_task to destroy measuring service for task: 11
[23117.247932] 
               –----------------------end----------------------------
```

### 4.7. TDM度量任务异常触发机器挂机流程（1.4固件版本后支持）

参考用例场景4测试主流程逻辑与场景1基本相同，主要调整为度量任务创建时flag配置增加TASK_EXCEPTION_CRASH属性配置，当度量异常触发时，机器将挂机，详细细节可阅读参考用例tdm_verify.c的代码逻辑。

```   
$ cd /lib/modules/`uname -r`/kernel/drivers/crypto/ccp/
```
test_scene表示测试场景，0表示正常创建场景测试，1表示更新操作场景，2表示防字典攻击场景，3表示虚拟地址任务场景，4表示异常挂机场景

安装本次测试模块
```
$ sudo insmod tdm-verify.ko test_scene=4
```
该命令运行后，系统触发度量任务异常，若机器此时挂机，表明该机制验证成功，仅可通过BMC硬重启或断电重启恢复，用户可根据verify的代码逻辑参考该机制的实现，否则该机制验证失败。

### 4.8. TDM AK证书获取与证书链验证流程（1.2固件版本后支持）

该场景演示了获取TDM的AK证书与验证AK证书的证书链的例子，通过该场景，可以熟悉并验证TDM模块的证书导出接口，同时通过hag完成对证书的导出、解析、验证来实现对整个TDM证书系统的了解。hag提供了证书获取以及从AK证书逐步验证整个证书链的过程，用户可以通过使用hag工具来熟悉TDM AK证书的使用。

基本验证流程如下：

（1）假设hag工具已安装到/usr/bin/目录（具体路径请以安装为准）

（2）检查hag支持的TDM命令
```
[root@localhost hygon]# hag tdm -help

get_ak_cert        get_tdm_report     get_vpcr_audit     show_tdm_device    
parse_ak_cert      verify_ak_cert     parse_tdm_report   verify_tdm_report  
parse_vpcr_audit   replay_vpcr_audit  

[tdm] Command successful!

```

（3）可以看到TDM支持get_ak_cert、parse_ak_cert、veriry_ak_cert三个与证书相关的命令，获取名字为cert的AK证书

结果如下所示，可以看到成功获取ak.cert的证书
```
[root@localhost hygon]# hag tdm get_ak_cert -out ak.cert
get tdm ak cert successful.
get_ak_cert command success!

[tdm] Command successful!
[root@localhost hygon]# ls -l
总用量 4
-rw-r--r-- 1 root root 448 9月   1 11:32 ak.cert
```

（4）解析证书，主要将证书中的版本、chip_id、curve_id、公钥信息、证书签名等信息进行解析显示，方便用户了解证书中的内容。
```
[root@localhost hygon]# hag tdm parse_ak_cert -in ak.cert
###########################TDM_CERT START##################################
version: 10000

chip_id_len:            13
chip_id:
0x4e 0x5a 0x47 0x46 0x47 0x30 0x36 0x31 0x30 0x31 0x38 0x30 0x35 
chip_id:                NZGFG06101805

curve_id:               3
qx:
0xb2 0xcd 0x0c 0x07 0x44 0x61 0x9d 0x97 0x00 0xd0 0xb9 0x72 0x65 0xe3 0x1a 0xba 
0x21 0x2d 0x66 0x40 0x51 0xe7 0xf6 0xac 0x2f 0x7d 0xcb 0x0c 0x17 0xd0 0x8b 0xb5 

qy:
0x4a 0x4b 0xca 0x88 0xcb 0x1d 0xb1 0x29 0x2f 0x4d 0x50 0xf6 0x5c 0xff 0xa8 0xdd 
0x1f 0xcb 0x4c 0x09 0x22 0xf7 0xe9 0x5b 0x9f 0xe9 0xd0 0x8a 0x5d 0x0f 0x45 0xcd 

user_id_len:            15
user_id:
0x48 0x59 0x47 0x4f 0x4e 0x2d 0x53 0x53 0x44 0x2d 0x54 0x44 0x4d 0x41 0x4b 

sig1_key_usage_id:      0x1004
sig1_r:
0x98 0x3d 0xeb 0x96 0x2e 0x6f 0xb8 0xcf 0xec 0x5a 0x0c 0x5a 0xaf 0xf1 0xb8 0x2d 
0xdc 0xaa 0x55 0x77 0x01 0xd0 0x74 0x1a 0x66 0x9e 0x60 0x3d 0xa6 0xf0 0xec 0x16 

sig1_s:
0xd5 0x09 0x57 0x7f 0x54 0x30 0x0e 0x8c 0x7c 0xf3 0x34 0x06 0xc4 0xa5 0xd1 0x46 
0xaf 0x67 0xbc 0x8d 0xb7 0x19 0xfd 0xb5 0xf1 0xdc 0x54 0x0a 0x41 0xde 0x16 0x51 

sig2_key_usage_id:      0x1000
sig2_r:
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

sig2_s:
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

ak cert origin data:
0x00 0x00 0x01 0x00 0x00 0x00 0x0d 0x00 0x4e 0x5a 0x47 0x46 0x47 0x30 0x36 0x31 
0x30 0x31 0x38 0x30 0x35 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x01 0x20 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x03 0x00 0x00 0x00 
0xb2 0xcd 0x0c 0x07 0x44 0x61 0x9d 0x97 0x00 0xd0 0xb9 0x72 0x65 0xe3 0x1a 0xba 
0x21 0x2d 0x66 0x40 0x51 0xe7 0xf6 0xac 0x2f 0x7d 0xcb 0x0c 0x17 0xd0 0x8b 0xb5 
0x4a 0x4b 0xca 0x88 0xcb 0x1d 0xb1 0x29 0x2f 0x4d 0x50 0xf6 0x5c 0xff 0xa8 0xdd 
0x1f 0xcb 0x4c 0x09 0x22 0xf7 0xe9 0x5b 0x9f 0xe9 0xd0 0x8a 0x5d 0x0f 0x45 0xcd 
0x0f 0x00 0x48 0x59 0x47 0x4f 0x4e 0x2d 0x53 0x53 0x44 0x2d 0x54 0x44 0x4d 0x41 
0x4b 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x04 0x10 0x00 0x00 
0x98 0x3d 0xeb 0x96 0x2e 0x6f 0xb8 0xcf 0xec 0x5a 0x0c 0x5a 0xaf 0xf1 0xb8 0x2d 
0xdc 0xaa 0x55 0x77 0x01 0xd0 0x74 0x1a 0x66 0x9e 0x60 0x3d 0xa6 0xf0 0xec 0x16 
0xd5 0x09 0x57 0x7f 0x54 0x30 0x0e 0x8c 0x7c 0xf3 0x34 0x06 0xc4 0xa5 0xd1 0x46 
0xaf 0x67 0xbc 0x8d 0xb7 0x19 0xfd 0xb5 0xf1 0xdc 0x54 0x0a 0x41 0xde 0x16 0x51 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x10 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

###########################TDM_CERT END##################################
parse tdm ak cert successful.
parse_ak_cert command success!

[tdm] Command successful!
```

（5）验证证书链
```
[root@localhost hygon]# hag tdm verify_ak_cert -in ak.cert
--2023-09-01 11:34:24--  https://cert.hygon.cn/hsk_cek?snumber=NZGFG06101805
正在解析主机 cert.hygon.cn (cert.hygon.cn)... 172.23.18.50
正在连接 cert.hygon.cn (cert.hygon.cn)|172.23.18.50|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
长度：2916 (2.8K) [binary]
正在保存至: “hsk_cek.cert”

hsk_cek.cert                                     100%[======================================================= \ 
===================================================>]   2.85K  --.-KB/s  用时 0s      

2023-09-01 11:34:24 (104 MB/s) - 已保存 “hsk_cek.cert” [2916/2916])

--2023-09-01 11:34:24--  https://cert.hygon.cn/hrk
正在解析主机 cert.hygon.cn (cert.hygon.cn)... 172.23.18.50
正在连接 cert.hygon.cn (cert.hygon.cn)|172.23.18.50|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
长度：832 [binary]
正在保存至: “hrk.cert”

hrk.cert                                         100%[======================================================= \ 
===================================================>]     832  --.-KB/s  用时 0s      

2023-09-01 11:34:24 (51.0 MB/s) - 已保存 “hrk.cert” [832/832])

hrk pubkey verify hrk cert successful
hrk pubkey verify hsk cert successful
hsk pubkey verify cek cert successful

[root@localhost hygon]# ls -l
总用量 20
-rw-r--r-- 1 root root  448 9月   1 11:32 ak.cert
-rw-r--r-- 1 root root 2084 9月   1 11:34 cek.cert
-rw-r--r-- 1 root root  832 9月   1 11:34 hrk.cert
-rw-r--r-- 1 root root 2916 9月   1 11:34 hsk_cek.cert
-rw-r--r-- 1 root root  832 9月   1 11:34 hsk.cert
```

（6）可以看到成功验证了TDM的证书链，该验证方式主要通过从AK证书中获取的chip_id，到证书服务器下载其对应的CEK证书、HSK证书、HRK证书，通过从根证书一级一级验证到AK证书。

### 4.9. TDM度量报告获取与使用AK证书验证度量报告流程（1.2固件版本后支持）

该场景演示了获取TDM度量报告与使用AK证书验证报告的例子，通过该场景，可以熟悉并验证TDM模块的度量报告导出接口，同时通过hag完成对报告的导出、解析、验证来实现对整个TDM度量报告系统的了解。hag工具提供了度量报告获取以及使用AK证书验证度量报告的过程，用户可以通过使用hag工具来熟悉TDM度量报告接口的使用。

基本验证流程如下：

（1）TDM支持get_tdm_report、parse_tdm_report、verify_tdm_report三个与报告相关的命令，其中get_tdm_report命令需要三个参数，第一个为报告名字，这里以report.bin为例，第二个为报告类型，分别为总结报告（0对应）与详细报告（1对应），第三个为用户提供的数据的文件名字，这里以user_data.bin为例，第四个为可选的任务号（默认为所有任务）。verify_tdm_report需要两个参数，第一个为验签的AK证书，第二个为被验证的度量报告文件。

（2）目前最新的tdm_verify经过调整后，可以支持TDM报告验证，调整为加载verify模块创建启动度量任务后不销毁，待到移除该模块时才销毁，因此加载verify模块后再调用度量报告获取命令可以获取实际TDM中运行的度量报告，这里以获取tdm_verify的场景1任务更新操作流程中的度量任务的详细报告为例进行说明。

（3）加载tdm_verify的场景1模块
```
$ sudo insmod tdm-verify.ko test_scene=1
```
在hag工具的目录下获取名为report.bin的详细度量报告，可以看到成功获取report.bin的证书

获取并查看用户提供的随机数文件：
```
$ dd if=/dev/random of=user_data.bin bs=32 count=1
$ hexdump user_data.bin
```
获取度量报告结果如下：
```
[root@localhost hygon]# hag tdm get_tdm_report -report_file report.bin -report_type 1 \ 
-user_data_file user_data.bin
######### Report type   : 1 #########
######### Report task id: 0xffffffff #########
get tdm report successful.
get_tdm_report command success!

[tdm] Command successful!
[root@localhost hygon]# ls -l
总用量 28
-rw-r--r-- 1 root root  448 9月   1 11:32 ak.cert
-rw-r--r-- 1 root root 2084 9月   1 11:34 cek.cert
-rw-r--r-- 1 root root  832 9月   1 11:34 hrk.cert
-rw-r--r-- 1 root root 2916 9月   1 11:34 hsk_cek.cert
-rw-r--r-- 1 root root  832 9月   1 11:34 hsk.cert
-rw-r--r-- 1 root root  448 9月   1 11:45 report.bin
-rw-r--r-- 1 root root   32 9月   1 11:45 user_data.bin
```

（4）解析度量报告，主要将报告中的版本、任务状态位映射、用户提供数据（与user_data.bin中数据相同）、度量任务哈希值、当前所有度量任务的状态信息（详细报告时才存在）进行解析显示，方便用户了解报告中的内容。

结果如下：
```
[root@localhost hygon]# hag tdm parse_tdm_report -report_file report.bin
###########################TDM_REPORT START##################################
###version:                     0x10000
###fw_version:                  1882
###report_type:                 1
###task_nums:                   3
###task_bitmap:
0xe0 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

###task_error_bitmap:
0xe0 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

###task_running_bitmap:
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

###user_supplied_data_len: 32
###user_supplied_data:
0x63 0xd6 0xae 0x11 0xd9 0x20 0xff 0x64 0xac 0x7a 0xd9 0x30 0xe4 0x9d 0x0a 0xeb 
0xad 0x84 0x0c 0x39 0x3a 0x8d 0x05 0x3c 0x0e 0x8d 0x08 0x76 0x30 0x5f 0x45 0xd3 

###aggregate_hash:
0x0f 0x68 0xc1 0x01 0x3e 0xd9 0x4b 0x33 0x7c 0x77 0xf8 0x1f 0xde 0x9f 0x49 0xa3 
0xff 0x76 0xad 0x40 0x4b 0xeb 0xe4 0x37 0xe4 0x80 0x35 0x5c 0x03 0x6e 0x34 0x60 

**************************************************
###task_id:                 15
###perios_ms:               0
###measured_count:          30
###last_measure_elapsed_ms: 32071
###measured_hash:
0x69 0x20 0xde 0x37 0x20 0xc7 0x9e 0x44 0xba 0x82 0x7b 0xcb 0x4a 0x72 0xc7 0xbd 
0xac 0xe6 0xd4 0xf2 0xe2 0xf5 0xd3 0x06 0x84 0x46 0x51 0x12 0x1f 0x8c 0xd7 0xde 

**************************************************
###task_id:                 16
###perios_ms:               0
###measured_count:          24
###last_measure_elapsed_ms: 32000
###measured_hash:
0x7d 0x49 0x86 0x8a 0x49 0xc5 0xdd 0xd3 0xa3 0x3b 0x6b 0x42 0x16 0x75 0xc6 0x87 
0x78 0xf9 0x16 0x36 0x33 0x91 0xc0 0x6e 0x47 0xbd 0x5f 0x55 0x21 0xba 0xcb 0xe9 

**************************************************
###task_id:                 17
###perios_ms:               0
###measured_count:          29
###last_measure_elapsed_ms: 31946
###measured_hash:
0xe8 0x45 0xd4 0x8e 0xc1 0x3f 0x0c 0xc8 0xf5 0x22 0x8b 0xf5 0xe2 0x25 0x5e 0x8e 
0xd7 0x9e 0xdd 0x04 0x72 0xcb 0xa4 0x0d 0x2b 0xa4 0xc4 0x4c 0x7d 0xe2 0xb5 0x3e 

**************************************************

###sig_key_usage_id:         0x2001
###sig_r:
0x3b 0x4a 0xd3 0xac 0xae 0xb0 0x8e 0x55 0xa9 0xb4 0xf3 0x5e 0x66 0x86 0xf4 0x23 
0xd2 0x5d 0x60 0x1a 0xee 0x02 0x88 0xea 0xc4 0x29 0xf0 0x17 0xcf 0x82 0x5c 0x01 

###sig_s:
0xc4 0x96 0x5f 0xfb 0xe9 0xfc 0x76 0xd7 0x6d 0xea 0xf5 0x65 0x83 0x7c 0x8f 0x0c 
0xa5 0xb1 0x1b 0x7e 0xae 0xde 0x9f 0x58 0xde 0xfd 0xb6 0xb0 0xd6 0x6f 0x13 0xc6 

report origin data:
0x00 0x00 0x01 0x00 0x5a 0x07 0x00 0x00 0x01 0x00 0x00 0x00 0x00 0x00 0x03 0x00 
0xe0 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0xe0 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x20 0x00 
0x63 0xd6 0xae 0x11 0xd9 0x20 0xff 0x64 0xac 0x7a 0xd9 0x30 0xe4 0x9d 0x0a 0xeb 
0xad 0x84 0x0c 0x39 0x3a 0x8d 0x05 0x3c 0x0e 0x8d 0x08 0x76 0x30 0x5f 0x45 0xd3 
0x0f 0x68 0xc1 0x01 0x3e 0xd9 0x4b 0x33 0x7c 0x77 0xf8 0x1f 0xde 0x9f 0x49 0xa3 
0xff 0x76 0xad 0x40 0x4b 0xeb 0xe4 0x37 0xe4 0x80 0x35 0x5c 0x03 0x6e 0x34 0x60 
0x0f 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x1e 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x47 0x7d 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x69 0x20 0xde 0x37 0x20 0xc7 0x9e 0x44 0xba 0x82 0x7b 0xcb 0x4a 0x72 0xc7 0xbd 
0xac 0xe6 0xd4 0xf2 0xe2 0xf5 0xd3 0x06 0x84 0x46 0x51 0x12 0x1f 0x8c 0xd7 0xde 
0x10 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x18 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x7d 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x7d 0x49 0x86 0x8a 0x49 0xc5 0xdd 0xd3 0xa3 0x3b 0x6b 0x42 0x16 0x75 0xc6 0x87 
0x78 0xf9 0x16 0x36 0x33 0x91 0xc0 0x6e 0x47 0xbd 0x5f 0x55 0x21 0xba 0xcb 0xe9 
0x11 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x1d 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0xca 0x7c 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0xe8 0x45 0xd4 0x8e 0xc1 0x3f 0x0c 0xc8 0xf5 0x22 0x8b 0xf5 0xe2 0x25 0x5e 0x8e 
0xd7 0x9e 0xdd 0x04 0x72 0xcb 0xa4 0x0d 0x2b 0xa4 0xc4 0x4c 0x7d 0xe2 0xb5 0x3e 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x01 0x20 0x00 0x00 
0x3b 0x4a 0xd3 0xac 0xae 0xb0 0x8e 0x55 0xa9 0xb4 0xf3 0x5e 0x66 0x86 0xf4 0x23 
0xd2 0x5d 0x60 0x1a 0xee 0x02 0x88 0xea 0xc4 0x29 0xf0 0x17 0xcf 0x82 0x5c 0x01 
0xc4 0x96 0x5f 0xfb 0xe9 0xfc 0x76 0xd7 0x6d 0xea 0xf5 0x65 0x83 0x7c 0x8f 0x0c 
0xa5 0xb1 0x1b 0x7e 0xae 0xde 0x9f 0x58 0xde 0xfd 0xb6 0xb0 0xd6 0x6f 0x13 0xc6 

###########################TDM_REPORT END##################################
parse tdm report successful.
parse_tdm_report command success!

[tdm] Command successful!
```

（5）验证报告

结果如下：
```
[root@localhost hygon]# hag tdm verify_tdm_report -ak_cert ak.cert -report_file report.bin
verify tdm report successful.
verify_tdm_report command success!

[tdm] Command successful!
```

（6）可以看到成功使用AK证书验证了度量报告，由于AK证书已在上一个场景中经过验证，可以确定是可信的，这里使用可信的AK继续验证度量报告，验证通过后，可以确定报告也是可信的，用户可以根据此报告获取到TDM内的各种状态信息；该演示为其中的一种场景，用户也可以根据自己需求，来获取其他的各种场景下的度量报告进行验证。

（7）卸载tdm_verify的模块，度量报告验证演示完成
```
$ sudo rmmod tdm-verify.ko
```

### 4.10. TDM VPCR获取与审计重放VPCR值验证流程（1.3固件版本后支持）

该场景演示了获取VPCR值以及VPCR审计信息的原始TDM度量值与fTPM原始PCR值获取，以及根据审计信息重放VPCR值的验证流程。通过该场景，可以熟悉并验证VPCR机制的应用，理解TDM利用fTPM的PCR实现VPCR从而利用fTPM成熟的PCR远程证明的过程。hag工具提供了VPCR审计信息获取的具体流程以及使用根据审计信息重放VPCR值的过程，用户可以通过使用hag来熟悉TDM基于VPCR的使用与审计信息接口的使用。

基本验证流程如下：

（1）TDM支持get_vpcr_audit、parse_vpcr_audit、replay_vpcr_audit三个与VPCR报告相关的命令，其中get_vpcr_audit命令需要两个参数，第一个为放置VPCR审计信息的文件名字，这里以audit.bin为例，第二个为需要获取对应PCR的编号，这里以PCR11为例。parse_vpcr_audit命令用来解析get_vpcr_audit命令获取的audit.bin文件，获取原始TDM度量任务度量值与fTPM的原始PCR值。replay_vpcr_audit命令用于重放VPCR，第一个参数为需要重放的VPCR文件，这里为audit.bin，第二个参数可选，为重放生成文件，可以根据需求选择。

（2）目前最新的tdm_verify经过调整后，可以支持VPCR的相关测试，目前tdm_verify共创建了三个度量任务，这里三个任务分别对应PCR编号的PCR10，PCR11，PCR12，这里以获取tdm_verify的场景1任务更新操作流程中对应PCR11的审计信息为例进行说明。

（3）首先通过tpm2_tools命令查看原始PCR的值（这里版本为tpm2_pcrread命令，其他版本可能为tpm2_pcrlist，根据机器装的tpm2_tools的版本确定）
```
[hygon@localhost hygon]$ tpm2_pcrread sm3_256
sm3_256:
  0 : 0x5D25A693796A9D6060834A9FB0AF416E9C9FB4D47A22326BBC45686300B471A3
  1 : 0xFAC21DA05E1F8467972D6ABAF2CBED26FBB81B20A26A2751D32798EE574A7B1F
  2 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  3 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  4 : 0xA2381A4E30198BED49FB10A3D86274930B10D0EE788ED1121D1B83CF814362C9
  5 : 0xC71BED60766F8B89F6296C076F88E702B0E0474ACB8019AC8B8DB52E66E739ED
  6 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  7 : 0x2304AF3530A51BC03051BA7D3A2BB7B462120DF1B1D13BB55FA0B565831C19F4
  8 : 0xDEA0758622845043428A74802AABB23B53941CD216BA934FBFB5AF4D69FD7905
  9 : 0x2A12748335078EF34ABB0803C293AD390C2D9AB8B6D5FA9086E6A1ED4CD706FD
  10: 0x0000000000000000000000000000000000000000000000000000000000000000
  11: 0x0000000000000000000000000000000000000000000000000000000000000000
  12: 0x0000000000000000000000000000000000000000000000000000000000000000
  13: 0x0000000000000000000000000000000000000000000000000000000000000000
  14: 0x0000000000000000000000000000000000000000000000000000000000000000
  15: 0x0000000000000000000000000000000000000000000000000000000000000000
  16: 0x8E4D2AD793AEDBA2DBFE2AE09F0A49727988DFE8F460923A75B524A7FE9CFD80
  17: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  18: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  19: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  20: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  21: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  22: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  23: 0x0000000000000000000000000000000000000000000000000000000000000000
```

（4）可以看到原始PCR11的值为全0，加载tdm_verify的场景1模块来创建带VPCR属性的TDM度量任务
```
$ sudo insmod tdm-verify.ko test_scene=1
```
重新查看PCR值
```
[hygon@localhost hygon]$ tpm2_pcrread sm3_256
sm3_256:
  0 : 0x5D25A693796A9D6060834A9FB0AF416E9C9FB4D47A22326BBC45686300B471A3
  1 : 0xFAC21DA05E1F8467972D6ABAF2CBED26FBB81B20A26A2751D32798EE574A7B1F
  2 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  3 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  4 : 0xA2381A4E30198BED49FB10A3D86274930B10D0EE788ED1121D1B83CF814362C9
  5 : 0xC71BED60766F8B89F6296C076F88E702B0E0474ACB8019AC8B8DB52E66E739ED
  6 : 0x0D72B0164E4FA67D6B43D3CB8EAD734737E479767E0D545EFF22C6FE6275B357
  7 : 0x2304AF3530A51BC03051BA7D3A2BB7B462120DF1B1D13BB55FA0B565831C19F4
  8 : 0xDEA0758622845043428A74802AABB23B53941CD216BA934FBFB5AF4D69FD7905
  9 : 0x2A12748335078EF34ABB0803C293AD390C2D9AB8B6D5FA9086E6A1ED4CD706FD
  10: 0x67B124D86989268B92A26FBB88CF440FD6157475BB5472A27ADEB0D77843FDB9
  11: 0xC4D7DDA8900CA0F784F36D9C8C95039244E842CB3FE73B5AFCCE521872D41D3F
  12: 0x2E2D1C8F7759D7CFB57004C007FC22EB3DB340AF7CAE90D5668E3AEA5A33F6D2
  13: 0x0000000000000000000000000000000000000000000000000000000000000000
  14: 0x0000000000000000000000000000000000000000000000000000000000000000
  15: 0x0000000000000000000000000000000000000000000000000000000000000000
  16: 0x8E4D2AD793AEDBA2DBFE2AE09F0A49727988DFE8F460923A75B524A7FE9CFD80
  17: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  18: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  19: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  20: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  21: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  22: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  23: 0x0000000000000000000000000000000000000000000000000000000000000000
```

（5）可以看到由于tdm_verify的三个任务分别对应PCR10、PCR11、PCR12，分别拓展出了VPCR的PCR10、PCR11、PCR12值。

（6）在hag工具的目录获取PCR11对应的TDM度量任务度量值与fTPM的原始PCR值，可以看到成功获取bin的审计文件。
```
[root@localhost hygon]# hag tdm get_vpcr_audit -audit_file audit.bin -pcr_num 11
######### PCR number   : 11 #########
get vpcr audit successful.
get_vpcr_audit command success!

[tdm] Command successful!
```

（7）解析审计信息，主要获取原始TPM的PCR值与PCR对应TDM度量任务的信息。可以看到TPM原始信息与先前获取到的原始信息一致。
```
[root@localhost hygon]# hag tdm parse_vpcr_audit -audit_file audit.bin
###########################TDM_VPCR_AUDIT START##################################
###pcr:                     11
###tpm2_digest:
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

**************************************************
###task_id:                 19
###hash:
0x7d 0x49 0x86 0x8a 0x49 0xc5 0xdd 0xd3 0xa3 0x3b 0x6b 0x42 0x16 0x75 0xc6 0x87 
0x78 0xf9 0x16 0x36 0x33 0x91 0xc0 0x6e 0x47 0xbd 0x5f 0x55 0x21 0xba 0xcb 0xe9 

**************************************************
###########################TDM_VPCR AUDIT END##################################
parse vpcr audit successful.
parse_vpcr_audit command success!

```

（8）重放VPCR，如下，重放出的VPCR值与PCR11的值一致，重放成功。
```
[root@localhost hygon]# hag tdm replay_vpcr_audit -vpcr_file audit.bin
###########################TDM_VPCR_AUDIT_REPLAY START##################################
###replay pcr:              11
VPCR hash:
0xc4 0xd7 0xdd 0xa8 0x90 0x0c 0xa0 0xf7 0x84 0xf3 0x6d 0x9c 0x8c 0x95 0x03 0x92 
0x44 0xe8 0x42 0xcb 0x3f 0xe7 0x3b 0x5a 0xfc 0xce 0x52 0x18 0x72 0xd4 0x1d 0x3f 

###########################TDM_VPCR_AUDIT_REPLAY END##################################
replay vpcr audit successful.
replay_vpcr_audit command success!

[tdm] Command successful!
```

（9）VPCR审计主要用来获取度量的详细信息且确保审计信息不被篡改，至此，VPCR属性的度量基本验证流程结束。

（10）卸载tdm_verify的模块
```
$ sudo rmmod tdm-verify.ko
``` 
