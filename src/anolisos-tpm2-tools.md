# 可信工具集
## tpm2-tools概述
tpm2-tools是一套基于TSS2.0（Trusted Software Stack，可信软件栈）接口开发的TPM2管理工具集合，可用于操作TPM2.0芯片实现密码学、可信存储、完整性验证等功能。
## 龙蜥Anolis OS上tpm2-tools实践
### Anolis OS上tpm2-tools安装与使用
#### 安装
根据以下命令在anolis（以Anolis 8.8为例）上安装tpm2-tools

```
yum install tpm2-tools
```

#### 使用
##### 说明
tpm2-tools工具可根据运行环境是否安装tpm2-abrmd工具决定tcti（Transmission Interface）类型，也可以通过--tcti参数修改tcti类型。
以tpm2_startup命令为例，未安装tpm2-abrmd，tpm2_startup通过device tcti与/dev/tpm0设备直接通信。
```
tpm2_startup -v
tool="tpm2_startup" version="" tctis="libtss2-tctildr" tcti-default=tcti-device
```
安装tpm2-abrmd后，tpm2_startup默认通过tabrmd tcti与tpm2-abrmd服务进程通信，由tpm2-abrmd负责与/dev/tpm0设备进行信息交互。
```
tpm2_startup -v
tool="tpm2_startup" version="" tctis="libtss2-tctildr" tcti-default=tcti-abrmd
```
##### 1、TPM2.0基本功能
tpm2_startup工具可执行TPM2_CC_Startup命令使能TPM2.0芯片
```
tpm2_startup  -V #执行TPM2_SU_STATE类型的startup
INFO on line: "54" in file: "tools/tpm2_startup.c": \ 
Sending TPM_Startup command with type: TPM2_SU_STATE

tpm2_startup -c -V #执行TPM2_SU_CLEAR类型的startup
INFO on line: "54" in file: "tools/tpm2_startup.c": \
Sending TPM_Startup command with type: TPM2_SU_CLEAR
```
tpm2_getcap工具可执行TPM2_CC_GetCapability命令获取TPM2.0芯片信息

```
tpm2_getcap algorithms -V #获取TPM2.0芯片支持的算法信息
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x0, property: 0x1
rsa:
  value:      0x1
  asymmetric: 1
  symmetric:  0
  hash:       0
  object:     1
  reserved:   0x0
  signing:    0
  encrypting: 0
  method:     0
.....

tpm2_getcap commands -V #获取TPM2.0芯片支持的命令码
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x2, property: 0x11f
TPM2_CC_NV_UndefineSpaceSpecial:
  value: 0x440011F
  commandIndex: 0x11f
  reserved1:    0x0
  nv:           1
  extensive:    0
  flushed:      0
  cHandles:     0x2
  rHandle:      0
  V:            0
  Res:          0x0
......

tpm2_getcap properties-fixed -V #获取TPM2.0芯片固定属性信息
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x6, property: 0x100
TPM2_PT_FAMILY_INDICATOR:
  raw: 0x322E3000
  value: "2.0"
TPM2_PT_LEVEL:
  raw: 0
TPM2_PT_REVISION:
  value: 1.16
TPM2_PT_DAY_OF_YEAR:
  raw: 0xF
TPM2_PT_YEAR:
  raw: 0x7E0
TPM2_PT_MANUFACTURER:
  raw: 0x564D5700
  value: "VMW"
.....

tpm2_getcap ecc-curves -V #获取TPM2.0芯片支持的椭圆曲线信息
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x8, property: 0x1
TPM2_ECC_NIST_P192: 0x1
TPM2_ECC_NIST_P224: 0x2
TPM2_ECC_NIST_P256: 0x3
TPM2_ECC_NIST_P384: 0x4
TPM2_ECC_BN_P256: 0x10
......

tpm2_getcap handles-nv-index -V #获取已定义的NV空间句柄
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x1, property: 0x1000000
- 0x1691D65
- 0x1C00002
- 0x1C0000A

tpm2_getcap handles-transient -V #获取暂存对象句柄
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x1, property: 0x80000000
- 0x80000000
- 0x80000001
```

##### 2、TPM2.0密码学功能
TPM2.0密钥管理采用加密存储的方式，每一密钥都有父密钥，密钥导出TPM2.0芯片时，都被父密钥加密保护，导入TPM2.0芯片时又父密钥解密恢复。在加密存储体系中，存在一个根密钥（又称为PrimaryObject），该密钥无法导出到TPM2.0芯片外。TPM2.0中有三个独立的特权域（Hierarchy），每一特权域都可创建根密钥。
###### 密钥创建
1）创建根密钥

```
tpm2_createprimary -C o -G rsa -c rsaprimary.ctx -V #在TPM_RH_Owner Hierary创建RSA算法的
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
name-alg:
  value: sha256
  raw: 0xb
attributes:
  value: fixedtpm|fixedparent|sensitivedataorigin|userwithauth \
  |restricted|decrypt
  raw: 0x30072
type:
  value: rsa
  raw: 0x1
exponent: 0x0
bits: 2048
scheme:
  value: null
  raw: 0x10
scheme-halg:
  value: (null)
  raw: 0x0
sym-alg:
  value: aes
  raw: 0x6
sym-mode:
  value: cfb
  raw: 0x43
sym-keybits: 128
rsa: b7a9f512d495edc54b0fae7a76c8f72a3708f0de4d6a6a08a73547c4d \ 
f6fddb15e5bf9a94fb5a63ecdeb62e18138d93be4d4522ac12a091b354bab5 \ 
e4e36dde30b17ae4e84bf5d72a5447f2bfb3e6bc53b9ba847d85c0ec016935 \ 
4e301dbd9d83ba45a43747d55b54152639786741116da666bfa2fa583e317f \ 
d1757309a1904c933fae6e92502a01b72bc3f46cc7665852b1a93d3b3344e9 \ 
5aa254ba4f7d9345916648a7a667a5ae275894a2789b46dff6a26cc8dc4cd8 \ 
3e848ac7e23a2fa7a0d2091eacb1cd40851eb0bdccb7ebdd1ad8057d1fbc1c \ 
be54ceacba3e4a90157cfa53adf22f88a7c730b4b1584dff596c62f88ade2a \ 
8a7c9d67f36f6db169b4f
INFO on line: "190" in file: "lib/files.c": \ 
Save TPMS_CONTEXT->savedHandle: 0x80000000
```
2）创建密钥

```
tpm2_create -C rsaprimary.ctx -G rsa -u rsa.public \ 
-r rsa.private -V #以上一步创建的PrimaryObject为父密钥，\
创建RSA算法的密钥
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c": \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000
name-alg:
  value: sha256
  raw: 0xb
attributes:
  value: fixedtpm|fixedparent|sensitivedataorigin|userwithauth \ 
  |decrypt|sign
  raw: 0x60072
type:
  value: rsa
  raw: 0x1
exponent: 0x0
bits: 2048
scheme:
  value: null
  raw: 0x10
scheme-halg:
  value: (null)
  raw: 0x0
sym-alg:
  value: null
  raw: 0x10
sym-mode:
  value: (null)
  raw: 0x0
sym-keybits: 0
rsa: d01e9a0f80a79c7248b29e66535a16c43ff0ad70f5f6773d048bb6e9178 \ 
78f91ac53f672091b8103123123bce8603d761e7b39eb12b4a286816068c40c4 \ 
af5bd6296bc565913acc69fa5b4485835f1493a180cfb41ec6d18828f195941a \ 
6446f55794ab8a304e78d2cf04e52d36a98ae94a70f8fa868dcbd8cf58c909df \ 
684f0dc1f41ba27bcd86097cb8ae0d3cc50d5fba3ea6efd5780a605536f8a60a \ 
a95350a0db6d639f5c25732ed4ab122df37d258d6786e0fbb123fc18eab71ed4 \ 
21c9200b1ebfc47ab5ab0e12a3566fcac5e97b1343ab022bf6ba8a94a1c4b795 \
46208806e3561d405bfdcbd7b2e7205a3fc73ed8e54cac847d32a06f0aec291e \ 
fb27f
```
注：由于TPM2.0芯片中存储空间有限，并不无限加载密钥，tpm2-tools在管理密钥方面，会将生成的密钥通过TPM2_CC_ContextSave将密钥信息导出到文件保存，当使用密钥时，先通过TPM2_CC_ContextLoad将密钥信息加载至芯片中，再使用该密钥。
###### RSA算法加密/解密

```
tpm2_create -C rsaprimary.ctx -G rsa -u rsa.public -r rsa.private #创建RSA算法的密钥

tpm2_load -C rsaprimary.ctx -u rsa.public -r rsa.private \ 
-c rsa-enc-key.ctx -V #执行TPM2_CC_Load命令将创建的RSA密钥加 \ 
载至TPM芯片中
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c":  \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000
name: 000b0b8d6e072c99c31c90856d9758ca1d2068147e028c \ 
8073914e4a17a85e573fca
INFO on line: "190" in file: "lib/files.c": \ 
Save TPMS_CONTEXT->savedHandle: 0x80000000

echo 12345 > data.txt #生成明文

tpm2_rsaencrypt -c rsa-enc-key.ctx -o cipher.bin data.txt -V \ 
#使用RSA密钥加密data.txt文件，将密文输出到cipher.bin文件中
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c": \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000

tpm2_rsadecrypt -c rsa-enc-key.ctx -o data-dec.txt cipher.bin -V \ 
#使用RSA密钥解密密文，并将密文输出到data-dec.txt文件
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c": \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000

diff data-dec.txt data.txt #明文与解密后文件对比
```

###### RSA算法签名/验签

```
tpm2_create -C rsaprimary.ctx -G rsa -u rsa.public \ 
-r rsa.private #创建RSA算法的密钥

tpm2_load -C rsaprimary.ctx -u rsa.public -r rsa.private \ 
-c rsa-sign-key.ctx -V #执行TPM2_CC_Load命令将创建的RSA密钥 \ 
加载至TPM芯片中

echo "rsasign" > rsasigndata.txt #生成签名内容

tpm2_sign -c rsa-sign-key.ctx -o rsa-sig.bin rsasigndata.txt  -V 
#使用RSA密钥对rsasigndata.txt签名，将签名信息写入rsa-sig.bin文件
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c": \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000

tpm2_verifysignature -c rsa-sign-key.ctx -s rsa-sig.bin \ 
-m rsasigndata.txt #使用RSA密钥验签

echo "rsasign1" > rsasign1data.txt #构建异常数据

tpm2_verifysignature -c rsa-sign-key.ctx -s rsa-sig.bin \ 
-m rsasign1data.txt -V #对异常数据签名验签
INFO on line: "362" in file: "lib/files.c": \ 
Assuming tpm context file
INFO on line: "293" in file: "lib/files.c": \ 
load: TPMS_CONTEXT->savedHandle: 0x80000000
WARNING:esys:src/tss2-esys/api/Esys_VerifySignature.c:302:Esys_VerifySignature_Finish() \ 
Received TPM Error
ERROR:esys:src/tss2-esys/api/Esys_VerifySignature.c:103:Esys_VerifySignature() \ 
Esys Finish ErrorCode (0x000002db)
ERROR on line: "53" in file: "lib/log.h": \ 
Esys_VerifySignature(0x2DB) - tpm:parameter(2):\
the signature is not valid
ERROR on line: "259" in file: "tools/tpm2_verifysignature.c": \
Verify signature failed!
ERROR on line: "147" in file: "tools/tpm2_tool.c": \
Unable to run tpm2_verifysignature
```

###### ECC算法签名/验签

```
tpm2_create -C rsaprimary.ctx -G ecc -u ecc.public -r ecc.private #创建ECC算法的密钥

tpm2_load -C rsaprimary.ctx -u ecc.public -r ecc.private -c ecc-sign-key.ctx -V \ 
#执行TPM2_CC_Load命令将创建的ECC密钥加载至TPM芯 片中

echo "eccsign" > eccsigndata.txt #生成签名内容

tpm2_sign -c ecc-sign-key.ctx -o ecc-sig.bin eccsigndata.txt  -V \ 
#使用ECC密钥对eccsigndata.txt签名，将签名信息写入ecc-sig.bin文件

tpm2_verifysignature -c ecc-sign-key.ctx -s ecc-sig.bin -m eccsigndata.txt #使用ECC密钥验签
```

##### 3、TPM2.0存储功能
TPM2.0芯片内置了NVRAM（Non-Volatile Random Access Memory，非易失性随机访问存储器），用于存放数据。TPM2.0芯片NVRAM读写需要授权，因此可用于存放敏感数据。TPM2.0 NV空间需要要先定义才能进行读写操作，使用完毕后要释放已定义的空间。

```
tpm2_nvdefine -C o -s 100 0x01800001 -V 
#在TPM_RH_Owner特权域中创建100字节的存储空间，空间索引为0x01800001
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
nv-index: 0x1800001

echo "1234567890" > nv.txt #生成存储数据

tpm2_nvwrite -i nv.txt -C o 0x01800001 -V \ 
#向NVRAM 0x01800001写入数据
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "80" in file: "tools/tpm2_nvwrite.c": \ 
The data(size=11) to be written:
INFO on line: "1657" in file: "lib/tpm2.c": \
Success to write NV area at index 0x1800001 offset 0x0.

tpm2_nvread -C o 0x01800001 -V \ 
#读取NVRAM 0x01800001中的内容
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
1234567890

tpm2_nvundefine -C o 0x01800001 -V #释放NVRAM 0x1800001
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "1580" in file: "lib/tpm2.c": \ 
Success to release NV area at index 0x1800001.
```
##### 4、TPM2.0 PCR功能
TPM2.0 PCR(Platform Configuration Register, 平台配置寄存器)是TPM2.0中与完整性相关的信息存储空间。PCR的更新方法叫做扩展（Extend），扩展是一种单向的加密操作，保证度量值不被篡改。

```
tpm2_pcrread -V #获取当前PCR中的内容
sha1:
  0 : 0xA660D212EE691C9295BBEA32A78BE89F9F27C5A9
  1 : 0xB2A83B0EBF2F8374299A5B2BDFC31EA955AD7236
  2 : 0xB2A83B0EBF2F8374299A5B2BDFC31EA955AD7236
  3 : 0xB2A83B0EBF2F8374299A5B2BDFC31EA955AD7236
  4 : 0xB0B51368B2865BD0B8B56BFE1CFE8E6177AB2465
  5 : 0x7273E316E323AAFFBFCDE3ED860DD266E0AA17EB
  6 : 0xB2A83B0EBF2F8374299A5B2BDFC31EA955AD7236
  7 : 0xDBAED2EAEFC85D2342AF7E2C7F0AD9188FF215B1
  8 : 0x16992E4CFBD1E29D5D99ADD56FC9AFAB1EFB0595
  9 : 0x3CFED51D0D507E2CDA5C996374BACAB93C6C6A16
  10: 0x327C45F6007C43E7C6D82958EE6D18F890796A02
  11: 0x0000000000000000000000000000000000000000
  12: 0x0000000000000000000000000000000000000000
  13: 0x0000000000000000000000000000000000000000
  14: 0x8DF12380EDE005407EAB81DA4405321E0DA61280
  15: 0x0000000000000000000000000000000000000000
  16: 0x0000000000000000000000000000000000000000
  17: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  18: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  19: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  20: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  21: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  22: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  23: 0x0000000000000000000000000000000000000000
sha256:
  0 : 0xCD77123A880A51DB10BBA64BEBFF3B0AD20BA4D50F9F8B8A6B341DBD4E02F468
  1 : 0x3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969
  2 : 0x3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969
  3 : 0x3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969
  4 : 0x76A09DC9FB1E61888B36E6C4A45C02A36B2E39FD40925506110F53586560D4B2
  5 : 0xD004B8D98FBEE7E967E9F46F55CDEE79D487FB5793AA5B1F6D7586A11AD9DEE9
  6 : 0x3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969
  7 : 0x592E7099CFF05155224F26EC6F3781975A7B095F151772CB61714E32F59F6DE1
  8 : 0xE8A441D072B34D68432E00F368BCC3113315CF3707475072BAB93E3195B474C9
  9 : 0x93889BC9C705243940156FAFBD8EDB6CF820962BC45E005BBFFC90C516E991B1
  10: 0x7ABD6A1ADAF3FA4AF27CAA9B541BDB79D535CB577C89FDED6BBF953ACB7AF29B
  11: 0x0000000000000000000000000000000000000000000000000000000000000000
  12: 0x0000000000000000000000000000000000000000000000000000000000000000
  13: 0x0000000000000000000000000000000000000000000000000000000000000000
  14: 0xA4DAD77FB3B6CACBD20F556986C5D917F5E322C123AF82D12C5E5B7EF7AE9938
  15: 0x0000000000000000000000000000000000000000000000000000000000000000
  16: 0x0000000000000000000000000000000000000000000000000000000000000000
  17: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  18: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  19: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  20: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  21: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  22: 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  23: 0x0000000000000000000000000000000000000000000000000000000000000000
  ......
  
echo "123" > pcr.txt #生成扩展数据
  
sha256sum pcr.txt #计算扩展数据sha256摘要值
181210f8f9c779c26da1d9b2075bde0127302ee0e3fca38c9a83f5b1dd8e5d3b   \
pcr.txt
 
tpm2_pcrextend 10:sha256=181210f8f9c779c26da1d9b2075bde0127302ee0e3fca38c9a83f5b1dd8e5d3b -V  \ 
#将pcr.txt摘要值扩展至PCR4 SHA-256 Bank中
```

##### 5、TPM2.0死锁功能
TPM2.0中对象都需要授权访问，使用错误授权访问具有DA保护属性的对象（如密钥、没有设置noDA属性的NV等）会导致死锁计数器加1，当死锁计数器达到一定数值后，TPM2.0便拒绝授权访问。
TPM2.0中与死锁相关的属性有maxTries(最大允许授权失败次数)、lockoutRecovery（Lockout Hierarchy死锁恢复时间），recoveryTime(死锁计数器自减一时间间隔)。

```
tpm2_getcap properties-variable -V #获取与DA相关的属性
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x6, property: 0x200
......
TPM2_PT_LOCKOUT_COUNTER: 0x0
TPM2_PT_MAX_AUTH_FAIL: 0x3
TPM2_PT_LOCKOUT_INTERVAL: 0x3E8
TPM2_PT_LOCKOUT_RECOVERY: 0x3E8
......

tpm2_dictionarylockout -s -l 300 -t 300 -n 10 -V \ 
#设定maxTries为10次，lockoutRecovery为300秒，recoveryTime为300秒
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "1110" in file: "lib/tpm2.c": \ 
Setting up Dictionary Lockout parameters.

tpm2_getcap properties-variable -V
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x6, property: 0x200
......
TPM2_PT_LOCKOUT_COUNTER: 0x0
TPM2_PT_MAX_AUTH_FAIL: 0xA
TPM2_PT_LOCKOUT_INTERVAL: 0x12C
TPM2_PT_LOCKOUT_RECOVERY: 0x12C
......

tpm2_dictionarylockout -c -V #重置死锁计数器
INFO on line: "44" in file: "lib/tpm2_capability.c": \ 
GetCapability: capability: 0x5, property: 0x0
INFO on line: "1099" in file: "lib/tpm2.c": \ 
Resetting dictionary lockout state.
```
