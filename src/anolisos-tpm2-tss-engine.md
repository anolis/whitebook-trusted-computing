# 3.5可信服务引擎
## 3.5.1 tpm2-tss-engine概述
tpm2-tss-engine利用遵循可信计算组织（Trusted Computing Group，TCG）的软件栈——TSS2.0,实现了基于TPM2设备的OpenSSL密码引擎。tpm2-tss-engine利用TSS2.0中的增强型系统API（Enhanced System Applicaion Service Interface， ESAPI）与TPM2设备通信。tpm2-tss-engine支持RSA加解密、签名以及ECDSA签名功能。
## 3.5.2 龙蜥Anolis OS上tpm2-tss-engine实践
龙蜥社区自其可信计算SIG成立以来，一直在关注可信计算业界进展和国际OSV厂商的可信计算方案。在完成tpm2-tss-engine的实践后，龙蜥社区也将自己的使用经验写入到白皮书中。未来，龙蜥社区除了继续加强与tpm2-tss-engine项目的交流与贡献外，还将结合自己在国密/国产化/云计算的积累围绕tpm2-tss-engine开展一些国密支持相关的工作，敬请期待。
### Anolis OS上tpm2-tss-engine安装与使用
#### 安装
根据以下命令在anolis（以Anolis 8.8为例）上安装tpm2-tss-engine

```shell
yum install git automake libtool autoconf autoconf-archive \ 
openssl-devel tpm2-tss-devel tpm2-tools make
git clone https://github.com/tpm2-software/tpm2-tss-engine.git
pushd tpm2-tss-engine
./bootstrap
./configure --prefix=/usr
make
make install
popd
```
#### 使用
##### 引擎信息

```
openssl engine -t -c tpm2tss
(tpm2tss) TPM2-TSS engine for OpenSSL
 [RSA, RAND]
     [ available ]
```
##### 随机数
基于TPM2生成128字节随机数

```
openssl rand -engine tpm2tss -hex 128
engine "tpm2tss" set.
8a1b6a489fcf1b1fd8324e97cd76ff7e52617373fc43f7227145c69163 \
b85bd15bb77375a4d5a69b998c4717e7b4c8b1bdb1f3b0e3936a6f528d \
9c90189c022cfeb94f008e35d54407c89229ef7fa338f9be0670e8d466 \
0aa61afcdb6e54dccd6079a9e2f93f3ce1528aa8124fcbbadd5bc79296 \
23ce2afe5802af2317b27a43

```
##### RSA算法功能
###### 创建密钥
tpm2tss-genkey创建密钥

```
tpm2tss-genkey -a rsa -s 2048 rsakey #使用tpm2-tss-genkey生成RSA算法密钥

openssl rsa -engine tpm2tss -inform engine -in rsakey -pubout \ 
-outform pem -out rsakey.pub #导出密钥公钥
engine "tpm2tss" set.
writing RSA key

cat rsakey.pub #读取公钥信息
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA17VGBbc3y8/+KSKJ5++K
MiGyY2CXpvgiYcajGZon8dEhYYLZ2d53wk6tgs19rHQl89T7h6rG2i5haaLRLTNr
gkxB4/OfK4dneVEtHgEZLbQmiGoI0ke4wCf9FhyrlpSRV7EUA0NYg86DE654X8Pd
4VsIc2Wb3Lf1MP1/lX/r5gZknyPqBe7NL5BM46m8WHS25tDf+Mg/vHADgWboVGFK
W+YpxYtubShAgOjXHc5lKuMKqG5nnIJkrxr8hgtf0ZXbVYywMt4NmaYV7Bc632ic
SkHk0OPGZ+RMl8YQmIEmXLK9Tu0IVy58dC0wxvi4V2GQ+p75uWF3K+nZmYUXFl+2
IQIDAQAB
-----END PUBLIC KEY-----
```
使用TPM2中已有RSA算法密钥

```
tpm2_createprimary -C o -G rsa2048 -c rsaprimary.ctx #创建TPM2 RSA算法Primary Object

tpm2_create -C rsaprimary.ctx -G rsa2048 -u rsa.pub -r rsa.pri #创建TPM2 RSA密钥

tpm2_load -C rsaprimary.ctx -u rsa.pub -r rsa.pri -c rsa.ctx #将RSA密钥导入TPM2芯片

tpm2_evictcontrol -C o -c rsa.ctx #将RSA密钥设置为持久对象
persistent-handle: 0x81000000
action: persisted

openssl rsa -engine tpm2tss -inform engine -in 0x81000000 \ 
-pubout -outform pem -out rsatpmkey.pub \ 
#导出TPM2中持久对象0x81000000的公钥
engine "tpm2tss" set.
Enter password for user key:
writing RSA key

cat rsatpmkey.pub ##读取公钥信息
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyA02SWyY6ZYyVR28O0R9
oDxxqd/RkxPa5W/4O773VkCRF8lovREKLrVV7pVHts6cxw8yrLc8Pzq5bArOTPh0
9M45Caxo13uhPd8H8p5UDORvrylJT7bJb5hrfJYyXyvd9FeXLqXexbJOSwPf+vcD
yv6OKccNwCK/3s//89aEm1B8xuYU1TXFnfo/sLJ+trUIiqrP3Aug/5gwB52lzTAX
WSCdogcbRL/AG7F2Zkn/56miZSzQ0I/o2Y/AaYrY3Oj0W/lIJmGTDiD5TbJJS3gQ
GdY1Tr3xf1Xsfo6ihJ0KCx2ZNBdtX7PIpErztLlllUCBGPNUt8OSVA+V6eZANv9B
owIDAQAB
-----END PUBLIC KEY-----
```
##### 加密/解密
使用tpm2tss-genkey生成的密钥加解密数据

```
echo 123456 > mydata #创建明文

openssl pkeyutl -pubin -inkey rsakey.pub -in mydata -encrypt \ 
-out mycipher #使用公钥加密数据

openssl pkeyutl -engine tpm2tss -keyform engine -inkey rsakey \ 
-decrypt -in mycipher -out mycipher-dec #使用私钥解密数据

diff mydata mycipher-dec #对比原文与解密后的明文
```
使用TPM2中已加载密钥加密数据

```
openssl pkeyutl -pubin -inkey rsatpmkey.pub -in mydata -encrypt \ 
-out mycipher #使用公钥加密数据

openssl pkeyutl -engine tpm2tss -keyform engine -inkey 0x81000000 \ 
-decrypt -in mycipher -out mycipher-dec 
#使用TPM2中持久对象0x81000000私钥解密数据

diff mydata mycipher-dec #对比原文与解密后的明文
```
##### 签名/验签
使用tpm2tss-genkey生成的密钥签名验签数据

```
openssl pkeyutl -engine tpm2tss -keyform engine -inkey rsakey \ 
-sign -in mydata -out mysig \ 
#使用tpm2tss-genkey生成的密钥rsakey签名数据

openssl pkeyutl -pubin -inkey rsakey.pub -verify -in mydata \ 
-sigfile mysig #使用rsakey的公钥验签
Signature Verified Successfully
```
使用TPM2中已加载密钥签名验签

```
openssl pkeyutl -engine tpm2tss -keyform engine -inkey 0x81000000 \ 
-sign -in mydata -out mysig #使用TPM2中持久对象0x81000000签名数据

openssl pkeyutl -pubin -inkey rsatpmkey.pub -verify -in mydata \ 
-sigfile mysig #使用TPM2中持久对象0x81000000的公钥验证签名
```
#### ECC算法功能
##### 创建密钥
tpm2tss-genkey创建密钥

```
tpm2tss-genkey -a ecdsa ecckey ##使用tpm2-tss-genkey生成ECC算法密钥，默认椭圆曲线为nist_p256

openssl ec -engine tpm2tss -inform engine -in ecckey -pubout \ 
-outform pem -out ecckey.pub #导出ECC密钥公钥

cat ecckey.pub #读取公钥信息
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwCUr6W94NwjHOQVoTdWQfxwXQ/qD
tuy2ZtDVL6yKkqnEJJZ0insTH+uJyeM0o3qeuKuzmlY+Qh053okXoA8t9w==
-----END PUBLIC KEY-----
```
使用TPM2中已有ECC算法密钥

```
tpm2_createprimary -C o -G ecc -c eccprimary.ctx \ 
#创建TPM2 ECC算法Primary Object

tpm2_create -C eccprimary.ctx -G ecc -u ecc.pub -r ecc.pri \ 
#创建TPM2 ECC密钥

tpm2_load -C eccprimary.ctx -u ecc.pub -r ecc.pri -c ecc.ctx 
#将ECC密钥导入TPM2芯片

tpm2_evictcontrol -C o -c ecc.ctx #将ECC密钥设置为持久对象
persistent-handle: 0x81000001
action: persisted

openssl ec -engine tpm2tss -inform engine -in 0x81000001 -pubout \ 
-outform pem -out ecctpmkey.pub #导出TPM2中持久对象0x81000000的公钥
engine "tpm2tss" set.
read EC key
Enter password for user key:
writing EC key

cat ecctpmkey.pub ##读取公钥信息
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEE16KNIBp/Ca7A3U38AKXcRh+Ji0O
dSzdfR/+ogYfN/NjvlW18IhKZg0rO2PdIsS2V5neCnffzKwRiVK0CP/Xvw==
-----END PUBLIC KEY-----

```
##### ECDSA密钥签名/验签
使用tpm2tss-genkey生成的密钥签名验签数据

```
echo 1234567890 > mydata #创建被签名信息

openssl dgst -sha256 -out mydata.sha256 -binary mydata 
#创建被签名信息的摘要值

openssl pkeyutl -engine tpm2tss -keyform engine -inkey ecckey -sign -in mydata.sha256 -out mysig \ 
#使用tpm2tss-genkey生成的密钥ecckey签名数据

openssl pkeyutl -engine tpm2tss -keyform engine -inkey ecckey -verify \ 
-in mydata.sha256 -sigfile mysig  
#使用tpm2tss-genkey生成的密钥ecckey验证签名数据
engine "tpm2tss" set.
Signature Verified Successfully
```
TPM2中已加载密钥签名验签

```
openssl pkeyutl -engine tpm2tss -keyform engine -inkey 0x81000001 \ 
-sign -in mydata.sha256 -out mysig #使用TPM2中持久对象0x81000001签名数据

openssl pkeyutl -engine tpm2tss -keyform engine -inkey 0x81000001 \ 
-verify -in mydata.sha256 -sigfile mysig
engine "tpm2tss" set.
Enter password for user key:
Signature Verified Successfully
```
#### X509证书功能
##### 自签名证书
使用TPM2中密钥生成自签名证书

```
tpm2_createprimary -C o -G rsa2048 -c rsaprimary.ctx #创建TPM2 RSA算法Primary Object

tpm2_create -C rsaprimary.ctx -G rsa2048 -u rsa.pub -r rsa.pri 
#创建TPM2 RSA密钥

tpm2_load -C rsaprimary.ctx -u rsa.pub -r rsa.pri -c rsa.ctx #将RSA密钥导入TPM2芯片

tpm2_evictcontrol -C o -c rsa.ctx #将RSA密钥设置为持久对象

persistent-handle: 0x81000000
action: persisted

openssl req -new -x509 -engine tpm2tss -keyform engine -key 0x81000000 \ 
-out rsa.crt#使用TPM2芯片中持久对象0x81000000生成自签名证书
engine "tpm2tss" set.
Enter password for user key:
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:CN
State or Province Name (full name) []:Shandong
Locality Name (eg, city) [Default City]:Jinan
Organization Name (eg, company) [Default Company Ltd]:XX
Organizational Unit Name (eg, section) []:Anolis
Common Name (eg, your name or your server's hostname) []:TC
Email Address []:

openssl x509 -in rsa.crt -text #查看证书详细信息
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            61:78:a3:3b:05:ec:e3:1a:ed:c0:a6:74:c5:ee:c6:60:22:7f:a5:53
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = CN, ST = Shandong, L = Jinan, O = XX, OU = Anolis, CN = TC
        Validity
            Not Before: Aug 25 16:48:29 2023 GMT
            Not After : Sep 24 16:48:29 2023 GMT
        Subject: C = CN, ST = Shandong, L = Jinan, O = XX, OU = Anolis, CN = TC
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:a8:27:60:bd:00:01:03:7c:d0:b4:4b:5e:44:92:
                    75:fa:84:5a:8a:80:ad:17:da:e0:6d:96:e9:4e:6f:
                    f6:b9:11:84:80:75:ab:66:2a:06:ce:db:59:8d:1f:
                    f9:11:54:ba:45:0a:cb:be:da:39:83:53:c8:9f:39:
                    9d:91:a7:37:03:9e:6a:dd:bd:89:86:e1:96:38:ff:
                    8d:c5:97:d1:1c:da:16:59:dc:98:c7:48:0b:ed:9f:
                    73:3f:b3:ac:8e:89:e2:c3:83:db:53:1d:9c:d3:a7:
                    f1:ea:33:97:f2:2c:98:04:a8:b1:e9:61:29:d5:78:
                    26:ad:d8:31:2f:d6:c6:c3:cf:87:63:4e:9d:2b:c0:
                    d1:67:b9:15:51:8a:4d:a7:46:98:fe:d9:83:10:91:
                    96:0e:54:cc:e7:77:05:73:0b:e9:f2:a0:18:b7:e4:
                    b9:98:96:90:58:8f:6e:e4:01:e6:7e:78:91:07:df:
                    04:2c:59:21:38:7e:05:56:27:2e:bf:af:77:d0:6c:
                    e6:8c:d9:97:f9:0e:58:65:b0:da:d3:6f:f3:33:e2:
                    c6:40:d3:9d:0c:ba:b6:78:5a:14:54:b1:89:09:9e:
                    5f:d4:86:a0:d0:09:41:fa:67:4c:48:02:96:a8:a5:
                    d5:f2:97:80:02:55:c1:b3:f2:b8:c2:32:82:1c:ed:
                    2a:d9
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                09:34:FF:5E:CE:1F:7E:42:C3:3B:59:DA:A7:74:68:B1:65:C7:4B:28
            X509v3 Authority Key Identifier:
                keyid:09:34:FF:5E:CE:1F:7E:42:C3:3B:59:DA:A7:74:68:B1:65:C7:4B:28

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         4d:02:e3:18:0f:63:1a:08:66:fb:b4:4a:86:f2:68:26:ce:bd:
         52:ac:e3:f3:95:fd:4f:44:31:f2:ce:40:33:61:a1:5e:59:67:
         76:c6:a8:4e:76:db:85:86:67:e4:ee:4c:fd:73:99:6c:12:21:
         bf:7a:71:b1:b4:ff:1a:ea:5a:f7:eb:3d:57:d7:d6:c7:73:db:
         dd:80:9f:95:ad:24:58:e5:dd:06:0a:47:c4:bc:22:2d:6c:54:
         99:1a:c9:6b:75:7e:a2:27:aa:cb:ab:4b:53:1b:be:33:08:7d:
         99:5d:67:4c:c7:4a:77:82:64:e1:30:3c:9d:17:be:88:a1:64:
         6a:c9:7e:ca:e5:48:f5:a2:cd:0e:8e:c9:9a:21:2c:fb:e4:56:
         ce:b1:cf:82:f4:b1:59:eb:a6:d8:0c:27:11:cb:2e:bf:d0:20:
         cc:d0:75:ef:12:af:34:2d:da:0d:cd:ea:a1:3c:0b:26:0f:0a:
         40:c6:9f:be:da:33:47:db:48:97:f5:5e:3b:4e:dd:3c:f8:d3:
         63:94:be:d4:98:c3:3f:8e:e7:71:85:30:71:1c:d4:0d:11:26:
         4c:ee:69:ce:18:2b:2c:16:8a:b8:02:9b:45:e9:ee:39:96:b4:
         76:93:56:e2:7c:c6:ab:1a:b0:89:c1:47:29:27:34:35:14:be:
         43:0e:92:16
-----BEGIN CERTIFICATE-----
MIIDlzCCAn+gAwIBAgIUYXijOwXs4xrtwKZ0xe7GYCJ/pVMwDQYJKoZIhvcNAQEL
BQAwWzELMAkGA1UEBhMCQ04xETAPBgNVBAgMCFNoYW5kb25nMQ4wDAYDVQQHDAVK
aW5hbjELMAkGA1UECgwCWFgxDzANBgNVBAsMBkFub2xpczELMAkGA1UEAwwCVEMw
HhcNMjMwODI1MTY0ODI5WhcNMjMwOTI0MTY0ODI5WjBbMQswCQYDVQQGEwJDTjER
MA8GA1UECAwIU2hhbmRvbmcxDjAMBgNVBAcMBUppbmFuMQswCQYDVQQKDAJYWDEP
MA0GA1UECwwGQW5vbGlzMQswCQYDVQQDDAJUQzCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAKgnYL0AAQN80LRLXkSSdfqEWoqArRfa4G2W6U5v9rkRhIB1
q2YqBs7bWY0f+RFUukUKy77aOYNTyJ85nZGnNwOeat29iYbhljj/jcWX0RzaFlnc
mMdIC+2fcz+zrI6J4sOD21MdnNOn8eozl/IsmASoselhKdV4Jq3YMS/WxsPPh2NO
nSvA0We5FVGKTadGmP7ZgxCRlg5UzOd3BXML6fKgGLfkuZiWkFiPbuQB5n54kQff
BCxZITh+BVYnLr+vd9Bs5ozZl/kOWGWw2tNv8zPixkDTnQy6tnhaFFSxiQmeX9SG
oNAJQfpnTEgClqil1fKXgAJVwbPyuMIyghztKtkCAwEAAaNTMFEwHQYDVR0OBBYE
FAk0/17OH35CwztZ2qd0aLFlx0soMB8GA1UdIwQYMBaAFAk0/17OH35CwztZ2qd0
aLFlx0soMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAE0C4xgP
YxoIZvu0SobyaCbOvVKs4/OV/U9EMfLOQDNhoV5ZZ3bGqE5224WGZ+TuTP1zmWwS
Ib96cbG0/xrqWvfrPVfX1sdz292An5WtJFjl3QYKR8S8Ii1sVJkayWt1fqInqsur
S1MbvjMIfZldZ0zHSneCZOEwPJ0XvoihZGrJfsrlSPWizQ6OyZohLPvkVs6xz4L0
sVnrptgMJxHLLr/QIMzQde8SrzQt2g3N6qE8CyYPCkDGn77aM0fbSJf1XjtO3Tz4
02OUvtSYwz+O53GFMHEc1A0RJkzuac4YKywWirgCm0Xp7jmWtHaTVuJ8xqsasInB
RyknNDUUvkMOkhY=
-----END CERTIFICATE-----
```
##### TLS服务器
通过tpm2-tss-engine可使用TPM2自签名证书创建TLS服务
```
openssl s_server -cert rsa.crt -key 0x81000000 -keyform engine \ 
-engine tpm2tss -accept 8443 #使用TPM2自签名证书创建SSL服务程序
```