
# 基于可信根的全盘加密
LUKS（Linux Unified Key Setup）是Linux硬盘加密的标准。LUKS是由Clemens Fruhwirth在2004年创建的磁盘加密规范，最初用于Linux，它是一种知名的、安全的、高性能的磁盘加密方法，基于改进版本的cryptsetup，使用dm-crypt作为磁盘加密后端。LUKS提供多种加密算法、多种加密模式和多种哈希函数可供选择，有40多种可能的组合。 
磁盘加密可以防止存储设备安装在由攻击者控制的备用操作环境中，攻击者可以观察或篡改敏感信息。 

本节以一个未受保护的loopback-mounted文件系统为例，介绍如何通过TPM增强基于luks磁盘加密的安全防护能力。[1]
## 未受保护的文件系统
为了便于理解、本文假设该loopback-mounted文件系统是操作系统在引导时自动挂载的逻辑磁盘分区。
```shell
# 创建磁盘镜像，写入内容
# 如下命令，我们在磁盘中添加了名为plain.txt的文件，该文件包含的内容为“this is my plain text”
dd if=/dev/zero of=plain.disk bs=1M count=10
mkfs.ext4 plain.disk
mkdir mountpoint
sudo mount plain.disk mountpoint
sudo sh -c 'echo "This is my plain text" > mountpoint/plain.txt'
sudo umount mountpoint

# 可以通过简单的linux命令(如下所示）即可查看文件内容
strings plain.disk
```
上述命令会泄露文件名和敏感文件内容。后面的小节将添加保护，以防止攻击者执行上面的简单攻击。
## 基于luks的磁盘数据加密
龙蜥操作系统支持基于luks加密的磁盘卷。LUKS可以保护静态数据的机密性。LUKS中配置信息一般存储在分区表头中，以方便迁移。LUKS卷可以自动挂载，默认的情况下加密密码口令可以通过人机交互的方式提供，或者通过指定为密钥文件(命令行参数)提供。cryptsetup用户态程序可用于创建和管理LUKS卷。
```shell
# 创建一个新的luks卷，使用简单的密码口令作为该卷的保护密钥
dd if=/dev/zero of=enc.disk bs=1M count=50
dd if=/dev/urandom of=disk.key bs=1 count=32
sudo losetup /dev/loop0 enc.disk
sudo cryptsetup --key-file=disk.key luksFormat /dev/loop0

# 基于上述方案加密后，磁盘中的文件不再可见、起到了一定的保护作用，采用如下命令无法查看明文信息
strings enc.disk | grep -i plain
```
基于luks加密磁盘卷的方法的最大问题是，如果密码口令或密钥文件以纯文本形式存储在磁盘上，则LUKS保护很容易被破坏。因此，许多LUKS实现以交互方式提示输入密码口令。但是在许多物联网(IoT)和嵌入式等场景中输入口令的方式不可行。人工输入的一种有效替代方法是将密码口令密封在锚定平台状态的安全令牌上，如TPM。 
## 基于TPM安全存储空间保护LUKS的密码口令
由于自动挂载需要在运行时在没有用户干预的情况下向cryptsetup提供密码口令或密钥，因此它必须在某种程度上清晰可用。需要一个方法来加密密码口令或密钥，以便在需要它时来解密卷。这种方法还必须防止磁盘与安装它的系统分离的情况。这种机制就是TPM。
该方案分为两个步骤:
- 将密码口令或密钥文件密封到TPM中。
- 在内存中解密秘密并将其传递给cryptsetup。

<a id='LUKS with passphrase stored in TPM'>**基于TPM保护luks口令安全**</a><br> 
```shell
# 创建并保存一个密封对象，并使用它来密封一个随机字节序列作为磁盘密钥:
tpm2_createprimary -Q --hierarchy=o --key-context=prim.ctx
dd if=/dev/urandom bs=1 count=32 status=none | tpm2_create --hash-algorithm=sha256 --public=seal.pub --private=seal.priv --sealing-input=- --parent-context=prim.ctx
tpm2_load -Q --parent-context=prim.ctx --public=seal.pub --private=seal.priv --name=seal.name --key-context=seal.ctx
tpm2_evictcontrol --hierarchy=o --object-context=seal.ctx 0x81010002

# 安装新密钥以取代旧密钥，并删除之前创建的旧密钥:
tpm2_unseal -Q --object-context=0x81010002 | sudo cryptsetup --key-file=disk.key luksChangeKey enc.disk 
shred disk.key
rm -f disk.key

# 用TPM中密封的新身份验证挂载卷:
sudo losetup /dev/loop0 enc.disk
tpm2_unseal -Q --object-context=0x81010002 | sudo cryptsetup --key-file=- luksOpen /dev/loop0 enc_volume
sudo mount /dev/mapper/enc_volume mountpoint

# 磁盘访问被授予新的秘密:
ls mountpoint

# 卸载磁盘：

sudo umount mountpoint
sudo cryptsetup remove enc_volume
sudo losetup -d /dev/loop0
```

该方案将防止通过克隆或物理窃取加密磁盘进行的离线攻击。攻击者现在还需要平台上的TPM和磁盘来访问数据，因为解密密钥不在磁盘上——它安全地存储在特定平台上的TPM上。
但是，攻击者仍然有可能从其他引导介质引导系统，执行上述命令，并检索解密LUKS卷所需的秘密。所需要的是一种可以锚定到受信任系统状态的身份验证机制。这可以通过TPM及其平台配置寄存器(PCR)密封提供来实现。

## 基于PCR增强授权保护带有密码口令的LUKS
TPM平台配置寄存器(PCR)用于防止攻击者更改系统引导参数或引导到其选择的操作系统以访问敏感数据。有效地使用TPM PCR需要系统固件、引导加载程序、操作系统内核和应用程序的配合。如果没有固件、驱动程序和软件来配置TPM, TPM将只是在I/O总线上处于非活动状态，什么也不做。
TPM PCR用于使用安全散列算法(如SHA-256)度量启动组件。当系统重置时，TPM PCR默认为零值。当系统启动时，关键系统组件(如固件、BIOS、OS加载程序等)的度量在启动过程中扩展到PCR中。扩展PCR是一个仅追加操作，并且需要对TPM进行I/O操作。因为不可能将PCR设置为用户指定的值，也不可能“收回”I/O，所以TPM PCR可以验证系统启动顺序，从而验证平台的状态，直到PCR度量停止为止。例如，PCR0包含系统固件和BIOS的度量，但不包含操作系统引导加载程序或内核。使用PCR0且仅使用PCR0的后果是，本示例只能防止固件替换攻击，前提是原始固件正确地进行了度量，并且替换固件不会将度量结果伪造给tpm(需要单独缓解的攻击)。

```
在实践中，有效的PCR集必须足够完整，以验证当前运行的代码和设计人员认为安全关键的预先运行的任何代码。《TCG PC Client Specific Implementation Specification for Conventional BIOS》仅指定了PCR 0-7、16和23的用法。操作系统引导期间和之后的PCR使用是特定于操作系统自定义的。
```
该方案有在上一方案的基础上增加了两个额外的步骤:
- 使用PCR作为TPM密封对象的身份验证授权代理（proxy authentication）。
- 在解密密钥文件后扩展密封PCR，使其不能在当前引导中再次解密。

```shell
# 在sha256 bank中创建一个当前值为PCR0的PCR策略:
tpm2_startauthsession --session=session.ctx
tpm2_policypcr -Q --session=session.ctx --pcr-list="sha256:0" --policy=pcr0.sha256.policy
tpm2_flushcontext session.ctx

# 现在将TPM非易失性内存中保护磁盘加密密钥的密封对象替换为一个新对象，该对象添加了我们刚刚创建的pcr策略，作为访问密封密钥的身份验证机制:
tpm2_unseal --object-context=0x81010002 | tpm2_create -Q --hash-algorithm=sha256 --public=pcr_seal_key.pub --private=pcr_seal_key.priv --sealing-input=- --parent-context=prim.ctx --policy=pcr0.sha256.policy
tpm2_evictcontrol --hierarchy=o --object-context=0x81010002 
tpm2_load -Q --parent-context=prim.ctx --public=pcr_seal_key.pub --private=pcr_seal_key.priv --name=pcr_seal_key.name --key-context=pcr_seal_key.ctx
tpm2_evictcontrol --hierarchy=o --object-context=pcr_seal_key.ctx 0x81010002 

# 现在尝试再次挂载加密磁盘，只不过这次密钥被密封在TPM对象中，其解密封操作只能通过满足PCR策略来访问。换句话说，通过PCR值所反映的预期系统软件状态不变来进行身份验证。
sudo losetup /dev/loop0 enc.disk
tpm2_startauthsession --policy-session --session=session.ctx
tpm2_policypcr -Q --session=session.ctx --pcr-list="sha256:0" --policy=pcr0.sha256.policy

# 此时，理想情况下，您希望在内存中解开秘密，并将其直接管道到cryptsetup，如下所示:“tpm2_unseal——auth=session:session。——object-context=0x81010002 | sudo cryptsetup luksOpen——key-file=- /dev/loop0 encvolume”。
# 但是，为了在下一节演示灵活PCR，我们将复制未密封的秘密:
tpm2_unseal --auth=session:session.ctx --object-context=0x81010002 > disk_secret.bkup
cat disk_secret.bkup | sudo cryptsetup --key-file=- luksOpen /dev/loop0 enc_volume
tpm2_flushcontext session.ctx
sudo mount /dev/mapper/enc_volume mountpoint/
ls mountpoint/

# 为了防止进一步开封，PCR0将被延长。这将导致PCR0保持不同的值，就像在固件替换攻击期间一样。这将导致策略检查失败，从而导致打开尝试失败。
# 延长前观察PCR状态，延长后再次观察:
tpm2_pcrread --sel-list=sha256:0
tpm2_pcrextend 0:sha256=0000000000000000000000000000000000000000000000000000000000000000
tpm2_pcrread --sel-list=sha256:0

# 尝试用脏PCR打开密封的磁盘加密密匙:
tpm2_startauthsession --policy-session --session=session.ctx
tpm2_policypcr -Q --session=session.ctx --pcr-list="sha256:0" --policy=pcr0.sha256.policy

# 以下操作将导致策略检查失败，从而阻止开封操作:
tpm2_unseal --auth=session:session.ctx --object-context=0x81010002
tpm2_flushcontext session.ctx

# 卸载磁盘：
sudo umount mountpoint
sudo cryptsetup remove enc_volume
sudo losetup -d /dev/loop0
```
该方案检索LUKS加密密码口令之前通过使用TPM PCR验证系统状态。
但是现在有一个新问题:更新。在本例中，对系统应用固件更新可能导致加密数据无法访问，因为PCR0表示固件的特定版本。为了成功地更新系统，有必要根据一组预测的PCR值重新密封秘密。但是，如果回滚更新，旧的值将不再工作。此外，在实际执行更新之前预测更新后的PCR值可能是不切实际的——最好的方法可能是在相同的系统上应用更新，看看会产生什么PCR值，并预先授权两组PCR值。对于TPM，有一种方法可以做到这一点，称为“被授权的PCR策略”（authorized PCR policy）。

## 基于被授权的PCR策略保护带有密码口令的LUKS

被授权的PCR策略作为TPM密封对象的身份验证机制。不再使用严格的PCR策略绑定到原始PCR值，我们现在密封它的PCR签名。PCR集由系统设计人员签名，并由TPM进行验证。 
这一方案可以通过以下步骤实现: 

<a id='Introduce a public/private RSA keypair'>**引入一个RSA公私钥对**</a><br> 
使用OpenSSL命令行工具生成RSA对。私钥将离线存储。公钥将与授权的PCR策略一起分发，并用作LUKS加密密码口令的授权机制。
```shell
openssl genrsa -out signing_key_private.pem 2048
openssl rsa -in signing_key_private.pem -out signing_key_public.pem -pubout
```
<a id='Sign a set of PCRs with the private key'>**用私钥签署一组PCRS**</a><br> 
在每个已知的良好系统配置上，收集当前的PCR值并用RSA私钥对它们进行签名。这个步骤可以根据需要进行多次。
```shell
tpm2_startauthsession --session=session.ctx
tpm2_policypcr -Q --session=session.ctx --pcr-list="sha256:0" --policy=set2.pcr.policy
tpm2_flushcontext session.ctx
openssl dgst -sha256 -sign signing_key_private.pem -out set2.pcr.signature set2.pcr.policy
```
<a id='Bind the LUKS encryption passphrase to the public key'>**将LUKS加密密码口令绑定到公钥**</a><br>
```shell
# 在将LUKS加密密码口令密封到TPM之前，有必要创建一个策略对象，该对象指定可以解封密码口令的条件。该策略将指定一组特定的pcr (PCR0)必须与使用特定密钥(signing_key_public.pem)签名的值匹配:
tpm2_loadexternal --key-algorithm=rsa --hierarchy=o --public=signing_key_public.pem --key-context=signing_key.ctx --name=signing_key.name
tpm2_startauthsession --session=session.ctx
tpm2_policyauthorize --session=session.ctx --policy=authorized.policy --name=signing_key.name
tpm2_flushcontext session.ctx

# 通过使用上述策略创建一个密封对象，将密码口令密封到TPM。请注意，由于前面的示例扩展了PCR0以防止密码口令的重新解密，因此使用了密码口令的备份副本:
cat disk_secret.bkup | tpm2_create --hash-algorithm=sha256 --public=auth_pcr_seal_key.pub --private=auth_pcr_seal_key.priv --sealing-input=- --parent-context=prim.ctx --policy=authorized.policy

# 用上面创建的对象替换旧的持久密封对象:
tpm2_evictcontrol --hierarchy=o --object-context=0x81010002 
tpm2_load -Q --parent-context=prim.ctx --public=auth_pcr_seal_key.pub --private=auth_pcr_seal_key.priv --name=auth_pcr_seal_key.name --key-context=auth_pcr_seal_key.ctx
tpm2_evictcontrol --hierarchy=o --object-context=auth_pcr_seal_key.ctx 0x81010002
```

<a id='Sign a set of PCRs with the private key'>**解密加密密码口令**</a>

```shell
# 加载公钥、PCR策略和签名，并要求TPM验证签名:
tpm2_loadexternal --key-algorithm=rsa --hierarchy=o --public=signing_key_public.pem --key-context=signing_key.ctx --name=signing_key.name
tpm2_verifysignature --key-context=signing_key.ctx --hash-algorithm=sha256 --message=set2.pcr.policy --signature=set2.pcr.signature --ticket=verification.tkt --format=rsassa

# 现在请TPM验证PCR值是否与当前值匹配，并为签名验证传递一个验证票据。请注意，一次只能验证一组PCR值:整个过程必须重复，以便尝试验证另一组签名PCR值:
tpm2_startauthsession --policy-session --session=session.ctx
tpm2_policypcr --pcr-list="sha256:0" --session=session.ctx --policy=set2.pcr.policy
tpm2_policyauthorize --session=session.ctx --input=set2.pcr.policy --name=signing_key.name --ticket=verification.tkt

# 解锁加密密码口令并解锁卷:
sudo losetup /dev/loop0 enc.disk
tpm2_unseal --auth=session:session.ctx --object-context=0x81010002 | sudo cryptsetup --key-file=- luksOpen /dev/loop0 enc_volume
tpm2_flushcontext session.ctx
sudo mount /dev/mapper/enc_volume mountpoint/
ls mountpoint/

# 卸载卷
sudo umount mountpoint
sudo cryptsetup remove enc_volume
sudo losetup -d /dev/loop0
```
[1]: 本方案的撰写参考了https://www.intel.com/content/www/us/en/developer/articles/code-sample/protecting-secret-data-and-keys-using-intel-platform-trust-technology.html

